package se.prv.jdbcsimulering.strings;

public class Constants {
	
	public static final String reference = "reference";
	public static final String symmetricKey = "symmetricalKey";
	public static final String exception = "exception";
	public static final String booleanReturnValue = "booleanReturnValue";
	public static final String integerReturnValue= "integerReturnValue";
	public static final String longReturnValue= "longReturnValue";
	public static final String shortReturnValue= "shortReturnValue";
	public static final String doubleReturnValue= "doubleReturnValue";
	public static final String stringReturnValue= "stringReturnValue";
	public static final String dateReturnValue = "dateReturnValue";
	public static final String objectReturnValue = "objectReturnValue";

}
