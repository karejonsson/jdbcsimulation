package se.prv.jdbcsimulering.utils;

public class GenerateSecret {

	public static String generate() {
		return GenerateReference.getRandom(8);
	}

}
