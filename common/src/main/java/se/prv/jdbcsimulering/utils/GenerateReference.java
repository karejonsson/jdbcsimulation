package se.prv.jdbcsimulering.utils;

public class GenerateReference {
	
	public static final String chars = "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ+/";
	
	public static String getRandom(int len) {
		StringBuffer out = new StringBuffer();
		for(int i = 0 ; i < len ; i++) {
			out.append(chars.charAt(((int) (Math.random()*chars.length()))));
		}
		return out.toString();
	}
	
	public static String generate() {
		return getRandom(20);
	}

}
