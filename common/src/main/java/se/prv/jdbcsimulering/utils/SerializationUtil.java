package se.prv.jdbcsimulering.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.Base64;

public class SerializationUtil {

	public static byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);   
			out.writeObject(obj);
			out.flush();
			byte[] bytesout = bos.toByteArray();
			return bytesout;
		} 
		finally {
			try {
				bos.close();
			} 
			catch (IOException ex) {
			}
		}		
	}
	
	public static String base64Encode(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}
	
	public static String base64(Object obj) throws IOException {
		return base64Encode(serialize(obj));
	}
	
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		ObjectInput in = null;
		try {
			in = new ObjectInputStream(bis);
			Object o = in.readObject(); 
			return o;
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} 
			catch (IOException ex) {
			}
		}	
	}

	public static byte[] base64Decode(String encoded) {
		return Base64.getDecoder().decode(encoded);
	}
	
	public static Object objectify(String s) throws IOException, ClassNotFoundException {
		return deserialize(base64Decode(s));
	}
	
}
