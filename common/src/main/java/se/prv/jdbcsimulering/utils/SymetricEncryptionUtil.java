package se.prv.jdbcsimulering.utils;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class SymetricEncryptionUtil {

	public static String encrypt(String message, String secret) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		return encrypt(message.getBytes(StandardCharsets.UTF_8), secret);
	}
	
	public static final String CypherInstanceName = "DES";

	public static String encrypt(byte[] cleartext, String secret) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		DESKeySpec keySpec = new DESKeySpec(secret.getBytes(StandardCharsets.UTF_8));
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(CypherInstanceName);
		SecretKey key = keyFactory.generateSecret(keySpec);
		Cipher cipher = Cipher.getInstance(CypherInstanceName); // cipher is not thread safe
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encrypted = cipher.doFinal(cleartext);
		String encodedString = Base64.getEncoder().encodeToString(encrypted);
		return encodedString;
	}
	
	public static String decrypt(String encodedString, String secret) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		byte[] encrypted = Base64.getDecoder().decode(encodedString);
		DESKeySpec keySpec = new DESKeySpec(secret.getBytes(StandardCharsets.UTF_8));
		SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(CypherInstanceName);
		SecretKey key = keyFactory.generateSecret(keySpec);
		Cipher cipher = Cipher.getInstance(CypherInstanceName); // cipher is not thread safe
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] plainTextPwdBytes = (cipher.doFinal(encrypted));
		return new String(plainTextPwdBytes, StandardCharsets.UTF_8);
	}
	
	public static void main(String[] args) throws InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		String secret = "pwd1b23a";
		String message = "Hej Hemlis, det är kul att vara hemlig, 1234567890";
		String s = encrypt(message, secret);
		System.out.println("Krypterat "+s);
		String m = decrypt(s, secret);
		System.out.println("M=<"+m+">");
	}

}
