package se.prv.jdbcwebservice.calls;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;  

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetHashMapPOST {

	public static final String httpResponseCode = "httpresponsecode";
	
	public static HttpURLConnection prepare(String url, String token) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestProperty("Authorization", token);
		con.setRequestMethod("POST");
		return con;
	}
	
	public static Map<String, Object> finish(HttpURLConnection con) {
		Integer recievedResponseCode = null;
		
		try {
			recievedResponseCode = con.getResponseCode();
			if (recievedResponseCode != 200) {
				throw new RuntimeException("Failed : HTTP error code : "+con.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(con.getInputStream())));

			StringBuffer response = new StringBuffer();
			String output;
			while ((output = br.readLine()) != null) {
				response.append(output);
			}
			System.out.println("-> "+response.toString());
			TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {};
			
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = mapper.readValue(response.toString(), typeRef);
			return map;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try { if(con != null) { con.disconnect(); }} catch(Exception e) {}
		}
		Map<String, Object> map = new HashMap<>();
		map.put(httpResponseCode, new Integer(recievedResponseCode != null ? recievedResponseCode : 500));
		return map;		
	}

	public static Map<String, Object> from_void(String url, String token) throws Exception {
		System.out.println("GetHashMap.from_void("+url+", token)");
		HttpURLConnection con = prepare(url, token);
		return finish(con);
	}
	
	public static Map<String, Object> from_string(String url, String token, String stringname, String stringvalue) throws Exception {
		System.out.println("GetHashMap.from_string("+url+", stringname="+stringname+", stringvalue="+stringvalue+")");
		HttpURLConnection con = prepare(url, token);
		con.setDoOutput(true);
		String urlParameters = stringname+"="+URLEncoder.encode(stringvalue, "UTF-8");//StandardCharsets.UTF_8);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		return finish(con);
	}

	public static Map<String, Object> from_string_string(String url, String token, String stringname1, String stringvalue1, String stringname2, String stringvalue2) throws Exception {
		System.out.println("GetHashMap.from_string("+url+", stringname1="+stringname1+", stringvalue1="+stringvalue1+", stringname2="+stringname2+", stringvalue2="+stringvalue2+")");
		HttpURLConnection con = prepare(url, token);
		con.setDoOutput(true);
		String urlParameters = 
				stringname1+"="+URLEncoder.encode(stringvalue1, "UTF-8")+//StandardCharsets.UTF_8)+
				"&"+stringname2+"="+URLEncoder.encode(stringvalue2, "UTF-8");//StandardCharsets.UTF_8);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		return finish(con);
	}

}
