package se.prv.jdbcwebservice.calls;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Map;

import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.utils.SerializationUtil;

public class CallsBySignature {

	public static String int_in_string_out(String token, String url, String argname, int arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (String) m.get(Constants.stringReturnValue);
	}

	public static int int_in_int_out(String token, String url, String argname, int arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (Integer) m.get(Constants.integerReturnValue);
	}
	
	public static double int_in_double_out(String token, String url, String argname, int arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.doubleReturnValue);
		return Double.valueOf(obj.toString());
	}
	
	public static double string_in_double_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.doubleReturnValue);
		return Double.valueOf(obj.toString());
	}
	
	public static long int_in_long_out(String token, String url, String argname, long arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.longReturnValue);
		return Long.valueOf(obj.toString());
	}
	
	public static long string_in_long_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.longReturnValue);
		return Long.valueOf(obj.toString());
	}
	
	public static short int_in_short_out(String token, String url, String argname, int arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.shortReturnValue);
		return Short.valueOf(obj.toString());
	}
	
	public static short string_in_short_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.shortReturnValue);
		return Short.valueOf(obj.toString());
	}
	
	public static String string_in_string_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (String) m.get(Constants.stringReturnValue);
	}

	public static int string_in_int_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (Integer) m.get(Constants.integerReturnValue);
	}

	public static boolean void_in_boolean_out(String token, String url) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_void(
					url, 
					token
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (Boolean) m.get(Constants.booleanReturnValue);
	}

	public static Date string_in_date_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.dateReturnValue);
		if(obj == null) {
			return null;
		}
		return new Date(Long.parseLong(obj.toString()));
	}

	public static Date int_in_date_out(String token, String url, String argname, int arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		Object obj = m.get(Constants.dateReturnValue);
		if(obj == null) {
			return null;
		}
		return new Date(Long.parseLong(obj.toString()));
	}

	public static Object int_in_object_out(String token, String url, String argname, int arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					String.valueOf(arg)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		String base64Ser = (String) m.get(Constants.objectReturnValue);
		if(base64Ser == null) {
			return null;
		}
		try {
			return SerializationUtil.objectify(base64Ser);
		} 
		catch (ClassNotFoundException e) {
			throw new SQLException(e);
		} 
		catch (IOException e) {
			throw new SQLException(e);
		}
	}

	public static Object string_in_object_out(String token, String url, String argname, String arg) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					arg
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		String base64Ser = (String) m.get(Constants.objectReturnValue);
		if(base64Ser == null) {
			return null;
		}
		try {
			return SerializationUtil.objectify(base64Ser);
		} 
		catch (ClassNotFoundException e) {
			throw new SQLException(e);
		} 
		catch (IOException e) {
			throw new SQLException(e);
		}
	}

	public static void void_in_void_out(String token, String url) throws SQLException {
		try {
			GetVoidPOST.from_void(url, token);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static int void_in_int_out(String token, String url) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_void(
					url, 
					token
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (Integer) m.get(Constants.integerReturnValue);
	}

	public static void int_byte_in_void_out(String token, String url, String argname1, int arg1, String argname2, byte arg2) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(arg1),
					argname2,
					String.valueOf(arg2)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_byteA_in_void_out(String token, String url, String argname1, int arg1, String argname2, byte[] arg2) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(arg1),
					argname2,
					String.valueOf(arg2)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_date_in_void_out(String token, String url, String argname1, int pos, String argname2, Date date) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1, 
					String.valueOf(pos), 
					argname2, 
					String.valueOf(date.getTime()));
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_double_in_void_out(String token, String url, String argname1, int pos, String argname2, double val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_float_in_void_out(String token, String url, String argname1, int pos, String argname2, float val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_int_in_void_out(String token, String url, String argname1, int pos, String argname2, int val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_long_in_void_out(String token, String url, String argname1, int pos, String argname2, long val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_string_in_void_out(String token, String url, String argname1, int pos, String argname2, String val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					val
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_int_in_void_out(String token, String url, String argname1, int pos, String argname2, Object obj) throws SQLException {
		try {
			String encodedString = Base64.getEncoder().encodeToString(SerializationUtil.serialize(obj));
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(encodedString)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_short_in_void_out(String token, String url, String argname1, int pos, String argname2, short val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void boolean_in_void_out(String token, String url, String argname, boolean arg) throws SQLException {
		try {
			GetVoidPOST.from_string(url, token, argname, String.valueOf(arg));
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void int_object_in_void_out(String token, String url, String argname1, int pos, String argname2, Object obj) throws SQLException {
		try {
			String encodedString = Base64.getEncoder().encodeToString(SerializationUtil.serialize(obj));
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					String.valueOf(pos),
					argname2,
					String.valueOf(encodedString)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_byte_in_void_out(String token, String url, String argname1, String arg1, String argname2, byte val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					arg1,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_byteA_in_void_out(String token, String url, String argname1, String name, String argname2, byte[] val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_date_in_void_out(String token, String url, String argname1, String name, String argname2, Date date) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(date.getTime())
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_double_in_void_out(String token, String url, String argname1, String name, String argname2, double val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_float_in_void_out(String token, String url, String argname1, String name, String argname2, float val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_int_in_void_out(String token, String url, String argname1, String name, String argname2, int val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_long_in_void_out(String token, String url, String argname1, String name, String argname2, long val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_string_in_void_out(String token, String url, String argname1, String name, String argname2, String val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					val
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_object_in_void_out(String token, String url, String argname1, String name, String argname2, Object obj) throws SQLException {
		try {
			String encodedString = Base64.getEncoder().encodeToString(SerializationUtil.serialize(obj));
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(encodedString)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static void string_short_in_void_out(String token, String url, String argname1, String name, String argname2, short val) throws SQLException {
		try {
			GetVoidPOST.from_string_string(
					url, 
					token,
					argname1,
					name,
					argname2,
					String.valueOf(val)
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
	}

	public static boolean string_in_boolean_out(String token, String url, String argname, String sql) throws SQLException {
		Map<String, Object> m = null;
		try {
			m = GetHashMapPOST.from_string(
					url, 
					token,
					argname,
					sql
					);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		SQLException sqle = (SQLException) m.get(Constants.exception);
		if(sqle != null) {
			throw sqle;
		}
		return (Boolean) m.get(Constants.booleanReturnValue);
	}

}
