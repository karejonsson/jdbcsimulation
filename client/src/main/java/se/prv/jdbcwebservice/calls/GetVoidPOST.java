package se.prv.jdbcwebservice.calls;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

public class GetVoidPOST {
	
	public static void finish(HttpURLConnection con) {
		Integer recievedResponseCode = null;
		
		try {
			recievedResponseCode = con.getResponseCode();
			if (recievedResponseCode != 200) {
				throw new RuntimeException("Failed : HTTP error code : "+recievedResponseCode);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));

			StringBuffer response = new StringBuffer();
			String output;
			while ((output = br.readLine()) != null) {
				response.append(output);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try { if(con != null) { con.disconnect(); }} catch(Exception e) {}
		}
	}

	public static void from_void(String url, String token) throws Exception {
		System.out.println("GetVoidPOST.from_void("+url+")");
		HttpURLConnection con = GetHashMapPOST.prepare(url, token);
		finish(con);
	}
	
	public static void from_string(String url, String token, String stringname, String stringvalue) throws Exception {
		System.out.println("GetVoidPOST.from_string("+url+", stringname="+stringname+", stringvalue="+stringvalue+")");
		HttpURLConnection con = GetHashMapPOST.prepare(url, token);
		con.setDoOutput(true);
		String urlParameters = 
				stringname+"="+URLEncoder.encode(stringvalue, "UTF-8");//StandardCharsets.UTF_8);
		System.out.println("urlParameters="+urlParameters);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		finish(con);
	}

	public static void from_string_string(String url, String token, String stringname1, String stringvalue1, String stringname2, String stringvalue2) throws Exception {
		System.out.println("GetVoidPOST.from_string_string("+url+", stringname1="+stringname1+", stringvalue1="+stringvalue1+", stringname2="+stringname2+", stringvalue2="+stringvalue2+")");
		HttpURLConnection con = GetHashMapPOST.prepare(url, token);
		con.setDoOutput(true);
		String urlParameters = 
				stringname1+"="+URLEncoder.encode(stringvalue1, "UTF-8")+//StandardCharsets.UTF_8)+
				"&"+stringname2+"="+URLEncoder.encode(stringvalue2, "UTF-8");//StandardCharsets.UTF_8);
		System.out.println("urlParameters="+urlParameters);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		finish(con);
	}

}
