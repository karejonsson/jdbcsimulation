package se.prv.jdbcwebservice.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

import org.apache.log4j.Logger;

import se.prv.jdbcwebservice.calls.CallsBySignature;
import se.prv.jdbcwebservice.calls.GetVoidPOST;

public class JDBCCStatement implements Statement {

	private static Logger logg = Logger.getLogger(JDBCCStatement.class); 

	protected String url_base = null;
	private String url_full = null;
	protected JDBCCConnection conn = null;
	protected String username = null;
	protected String jwtsecret = null;
	protected long validity;
	protected String reference = null;
	
	public JDBCCStatement(String url, JDBCCConnection conn, String stmtreference) {
		this.url_base = url;
		this.url_full = url+"/Statement";
		this.conn = conn;
		this.username = conn.getUsername();
		this.jwtsecret = conn.getJwtsecret();
		this.validity = conn.getValidity();
		this.reference = stmtreference;
	}

	public String toString() {
		return "JDBCCStatement{url_full="+url_full+", username="+username+", reference="+reference+"}";
	}
	
	public String getUsername() {
		return username;
	}
	
	public String getJwtsecret() {
		return jwtsecret;
	}
	
	public long getValidity() {
		return validity;
	}
	
	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void addBatch(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void cancel() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/cancel");
	}

	@Override
	public void clearBatch() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/clearBatch");
	}

	@Override
	public void clearWarnings() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/clearWarnings");
	}

	@Override
	public void close() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/close");
	}

	@Override
	public void closeOnCompletion() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/closeOnCompletion");
	}

	@Override
	public boolean execute(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int[] executeBatch() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet executeQuery(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Connection getConnection() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getFetchDirection() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getFetchSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getMaxRows() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getMoreResults(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetType() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getUpdateCount() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isPoolable() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setEscapeProcessing(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setMaxFieldSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setPoolable(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

}
