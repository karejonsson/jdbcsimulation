package se.prv.jdbcwebservice.sql;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcwebservice.calls.CallsBySignature;
import se.prv.jdbcwebservice.calls.ManagedCall;

public class JDBCCResultSet implements ResultSet {
	
	private static Logger logg = Logger.getLogger(JDBCCPreparedStatement.class); 
	
	private String url_base = null;
	private String url_fullrs = null;
	protected JDBCCStatement stmt = null;
	protected String username = null;
	protected String jwtsecret = null;
	protected long validity;
	protected String reference = null;
	
	public JDBCCResultSet(String url, JDBCCStatement stmt, String resultsetreference) {
		this.url_base = url;
		this.url_fullrs = url+"/ResultSet";
		this.stmt = stmt;
		this.username = stmt.getUsername();
		this.jwtsecret = stmt.getJwtsecret();
		this.validity = stmt.getValidity();
		this.reference = resultsetreference;
	}
	
	public String toString() {
		return "JDBCCResultSet{url_fullrs="+url_fullrs+", username="+username+", reference="+reference+"}";
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean absolute(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void afterLast() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void beforeFirst() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void cancelRowUpdates() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void clearWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void close() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void deleteRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public int findColumn(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean first() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Array getArray(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Array getArray(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public InputStream getAsciiStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public InputStream getAsciiStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public InputStream getBinaryStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public InputStream getBinaryStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Blob getBlob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Blob getBlob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getBoolean(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getBoolean(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte getByte(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte getByte(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte[] getBytes(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte[] getBytes(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getCharacterStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getCharacterStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Clob getClob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Clob getClob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getConcurrency() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getCursorName() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Date getDate(int pos) throws SQLException {
		return CallsBySignature.int_in_date_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullrs+"/getDate", "pos", pos);
	}

	@Override
	public Date getDate(String name) throws SQLException {
		return CallsBySignature.string_in_date_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullrs+"/getDateName", "name", name);
	}

	@Override
	public Date getDate(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Date getDate(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public double getDouble(int pos) throws SQLException {
		return CallsBySignature.int_in_double_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullrs+"/getDouble", "pos", pos);
	}

	@Override
	public double getDouble(String name) throws SQLException {
		return CallsBySignature.string_in_double_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullrs+"/getDoubleName", "name", name);
	}

	@Override
	public int getFetchDirection() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getFetchSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public float getFloat(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public float getFloat(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}
	
	@Override
	public int getInt(int pos) throws SQLException {
		return CallsBySignature.int_in_int_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getInt", "pos", pos);
	}

	@Override
	public int getInt(String name) throws SQLException {
		return CallsBySignature.string_in_int_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getIntName", "name", name);
	}

	@Override
	public long getLong(int pos) throws SQLException {
		return CallsBySignature.int_in_long_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getLong", "pos", pos);
	}

	@Override
	public long getLong(String name) throws SQLException {
		return CallsBySignature.string_in_long_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getLongName", "name", name);
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		String rsmdreference = ManagedCall.getReference(url_fullrs+"/getMetaData", reference, username, jwtsecret, validity);
		return new JDBCCResultSetMetaData(url_base, this, rsmdreference);
	}

	@Override
	public Reader getNCharacterStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getNCharacterStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public NClob getNClob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public NClob getNClob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getNString(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getNString(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Object getObject(int pos) throws SQLException {
		return CallsBySignature.int_in_object_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getObject", "pos", pos);
	}

	@Override
	public Object getObject(String name) throws SQLException {
		return CallsBySignature.string_in_object_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getObjectName", "name", name);
	}

	@Override
	public Object getObject(int arg0, Map<String, Class<?>> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Object getObject(String arg0, Map<String, Class<?>> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T getObject(int arg0, Class<T> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T getObject(String arg0, Class<T> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Ref getRef(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Ref getRef(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public RowId getRowId(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public RowId getRowId(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLXML getSQLXML(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLXML getSQLXML(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public short getShort(int pos) throws SQLException {
		return CallsBySignature.int_in_short_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getShort", "pos", pos);
	}

	@Override
	public short getShort(String name) throws SQLException {
		return CallsBySignature.string_in_short_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getShortName", "name", name);
	}

	@Override
	public Statement getStatement() throws SQLException {
		return stmt;
	}
	
	@Override
	public String getString(int pos) throws SQLException {
		return CallsBySignature.int_in_string_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getString", "pos", pos);
	}

	@Override
	public String getString(String name) throws SQLException {
		return CallsBySignature.string_in_string_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/getStringName", "name", name);
	}

	@Override
	public Time getTime(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getType() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public URL getURL(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public URL getURL(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public InputStream getUnicodeStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public InputStream getUnicodeStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void insertRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public boolean isAfterLast() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isBeforeFirst() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isFirst() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isLast() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean last() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void moveToCurrentRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	} 

	@Override
	public void moveToInsertRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public boolean next() throws SQLException {
		return CallsBySignature.void_in_boolean_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/next");
	}

	@Override
	public boolean previous() throws SQLException {
		return CallsBySignature.void_in_boolean_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrs+"/previous");
	}

	@Override
	public void refreshRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean relative(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean rowDeleted() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean rowInserted() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean rowUpdated() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateArray(int arg0, Array arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateArray(String arg0, Array arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateAsciiStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateAsciiStream(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateAsciiStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateAsciiStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void updateBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void updateBigDecimal(String arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBinaryStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBlob(int arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBlob(String arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBlob(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBlob(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBlob(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBoolean(int arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBoolean(String arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateByte(int arg0, byte arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateByte(String arg0, byte arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBytes(int arg0, byte[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateBytes(String arg0, byte[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateClob(int arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateClob(String arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateClob(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateClob(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateDate(int arg0, Date arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateDate(String arg0, Date arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateDouble(int arg0, double arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateDouble(String arg0, double arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateFloat(int arg0, float arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateFloat(String arg0, float arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateInt(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateInt(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateLong(int arg0, long arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateLong(String arg0, long arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNCharacterStream(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNClob(int arg0, NClob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNClob(String arg0, NClob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNClob(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNClob(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNString(int arg0, String arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNString(String arg0, String arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNull(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateNull(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateObject(int arg0, Object arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateObject(String arg0, Object arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateObject(int arg0, Object arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateObject(String arg0, Object arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateRef(int arg0, Ref arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateRef(String arg0, Ref arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateRow() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateRowId(int arg0, RowId arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateRowId(String arg0, RowId arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateSQLXML(int arg0, SQLXML arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateSQLXML(String arg0, SQLXML arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");	
	}

	@Override
	public void updateShort(int arg0, short arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateShort(String arg0, short arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateString(int arg0, String arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateString(String arg0, String arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateTime(int arg0, Time arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateTime(String arg0, Time arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateTimestamp(int arg0, Timestamp arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void updateTimestamp(String arg0, Timestamp arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public boolean wasNull() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	public String getUsername() {
		return username;
	}

	public String getJwtsecret() {
		return jwtsecret;
	}

	public long getValidity() {
		return validity;
	}

}
