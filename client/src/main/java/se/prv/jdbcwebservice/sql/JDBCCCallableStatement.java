package se.prv.jdbcwebservice.sql;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcwebservice.calls.CallsBySignature;
import se.prv.jdbcwebservice.calls.ManagedCall;

public class JDBCCCallableStatement extends JDBCCPreparedStatement implements CallableStatement {

	private static Logger logg = Logger.getLogger(JDBCCCallableStatement.class); 
	
	private String url_fullcs = null;
	
	public JDBCCCallableStatement(String url, JDBCCConnection conn, String stmtreference) {
		super(url, conn, stmtreference);
		this.url_fullcs = url+"/CallableStatement";
	}
	
	public String toString() {
		return "JDBCCCallableStatement{url_fullcs="+url_fullcs+", username="+username+", reference="+reference+"}";
	}

	@Override
	public void addBatch() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");

	}

	@Override
	public void clearParameters() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");

	}

	@Override
	public boolean execute() throws SQLException {
		return CallsBySignature.void_in_boolean_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/execute");
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		String rsreference = ManagedCall.getReference(url_fullcs+"/executeQuery", reference, username, jwtsecret, validity);
		return new JDBCCResultSet(url_base, this, rsreference);
	}

	@Override
	public int executeUpdate() throws SQLException {
		return CallsBySignature.void_in_int_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/executeUpdate");
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setArray(int arg0, Array arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBlob(int arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBlob(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setByte(int arg0, byte arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBytes(int arg0, byte[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClob(int arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setDate(int pos, Date date) throws SQLException {
		CallsBySignature.int_date_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setDate", "pos", pos, "valmillis", date);
	}

	@Override
	public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setDouble(int pos, double val) throws SQLException {
		CallsBySignature.int_double_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setDouble", "pos", pos, "val", val);
	}

	@Override
	public void setFloat(int pos, float val) throws SQLException {
		CallsBySignature.int_float_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setFloat", "pos", pos, "val", val);
	}

	@Override
	public void setInt(int pos, int val) throws SQLException {
		CallsBySignature.int_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setFloat", "pos", pos, "val", val);
	}

	@Override
	public void setLong(int pos, long val) throws SQLException {
		CallsBySignature.int_long_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setLong", "pos", pos, "val", val);
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNClob(int arg0, NClob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNString(int pos, String val) throws SQLException {
		CallsBySignature.int_string_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setNString", "pos", pos, "val", val);
	}

	@Override
	public void setNull(int pos, int type) throws SQLException {
		CallsBySignature.int_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setNull", "pos", pos, "type", type);
	}

	@Override
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setObject(int pos, Object obj) throws SQLException {
		CallsBySignature.int_object_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setObject", "pos", pos, "obj", obj);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setRef(int arg0, Ref arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setRowId(int arg0, RowId arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setShort(int pos, short val) throws SQLException {
		CallsBySignature.int_short_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setShort", "pos", pos, "val", val);
	}

	@Override
	public void setString(int pos, String val) throws SQLException {
		CallsBySignature.int_string_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setString", "pos", pos, "val", val);
	}

	@Override
	public void setTime(int arg0, Time arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setURL(int arg0, URL arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setUnicodeStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void addBatch(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void cancel() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/cancel");
	}

	@Override
	public void clearBatch() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/clearBatch");
	}

	@Override
	public void clearWarnings() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/clearWarnings");
	}

	@Override
	public void close() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/close");
	}

	@Override
	public void closeOnCompletion() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/closeOnCompletion");
	}

	@Override
	public boolean execute(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int[] executeBatch() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet executeQuery(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Connection getConnection() throws SQLException {
		return conn;
	}

	@Override
	public int getFetchDirection() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getFetchSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getMaxRows() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getMoreResults(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetType() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getUpdateCount() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isPoolable() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setEscapeProcessing(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setMaxFieldSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setPoolable(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Array getArray(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Array getArray(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public BigDecimal getBigDecimal(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Blob getBlob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Blob getBlob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getBoolean(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getBoolean(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte getByte(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte getByte(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte[] getBytes(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public byte[] getBytes(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getCharacterStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getCharacterStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Clob getClob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Clob getClob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Date getDate(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Date getDate(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Date getDate(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Date getDate(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public double getDouble(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public double getDouble(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public float getFloat(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public float getFloat(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getInt(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getInt(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public long getLong(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public long getLong(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getNCharacterStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Reader getNCharacterStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public NClob getNClob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public NClob getNClob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getNString(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getNString(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Object getObject(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Object getObject(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Object getObject(int arg0, Map<String, Class<?>> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Object getObject(String arg0, Map<String, Class<?>> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T getObject(int arg0, Class<T> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T getObject(String arg0, Class<T> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Ref getRef(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Ref getRef(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public RowId getRowId(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public RowId getRowId(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLXML getSQLXML(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLXML getSQLXML(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public short getShort(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public short getShort(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getString(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getString(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Time getTime(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Timestamp getTimestamp(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public URL getURL(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public URL getURL(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void registerOutParameter(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void registerOutParameter(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void registerOutParameter(int arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void registerOutParameter(int arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void registerOutParameter(String arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void registerOutParameter(String arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setAsciiStream(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setAsciiStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setAsciiStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBigDecimal(String arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBinaryStream(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBinaryStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBinaryStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBlob(String arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBlob(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBlob(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setBoolean(String arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setByte(String name, byte val) throws SQLException {
		CallsBySignature.string_byte_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setByteName", "name", name, "val", val);
	}

	@Override
	public void setBytes(String name, byte[] val) throws SQLException {
		CallsBySignature.string_byteA_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setBytesName", "name", name, "val", val);
	}

	@Override
	public void setCharacterStream(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCharacterStream(String arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClob(String arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClob(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClob(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setDate(String name, Date date) throws SQLException {
		CallsBySignature.string_date_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setDateName", "name", name, "valmillis", date);
	}

	@Override
	public void setDate(String arg0, Date arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setDouble(String name, double val) throws SQLException {
		CallsBySignature.string_double_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setDoubleName", "name", name, "val", val);
	}

	@Override
	public void setFloat(String name, float val) throws SQLException {
		CallsBySignature.string_float_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setFloatName", "name", name, "val", val);
	}

	@Override
	public void setInt(String name, int val) throws SQLException {
		CallsBySignature.string_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setIntName", "name", name, "val", val);
	}

	@Override
	public void setLong(String name, long val) throws SQLException {
		CallsBySignature.string_long_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setLongName", "name", name, "val", val);
	}

	@Override
	public void setNCharacterStream(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNClob(String arg0, NClob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNClob(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNClob(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNString(String name, String val) throws SQLException {
		CallsBySignature.string_string_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setNString", "name", name, "val", val);
	}

	@Override
	public void setNull(String name, int type) throws SQLException {
		CallsBySignature.string_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setNullName", "name", name, "type", type);
	}

	@Override
	public void setNull(String arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setObject(String name, Object obj) throws SQLException {
		CallsBySignature.string_object_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setObjectName", "name", name, "obj", obj);
	}

	@Override
	public void setObject(String arg0, Object arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setObject(String arg0, Object arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setRowId(String arg0, RowId arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setSQLXML(String arg0, SQLXML arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setShort(String name, short val) throws SQLException {
		CallsBySignature.string_short_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setShortName", "name", name, "val", val);
	}

	@Override
	public void setString(String name, String val) throws SQLException {
		CallsBySignature.string_string_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullcs+"/setStringName", "name", name, "val", val);
	}

	@Override
	public void setTime(String arg0, Time arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTime(String arg0, Time arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTimestamp(String arg0, Timestamp arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTimestamp(String arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setURL(String arg0, URL arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean wasNull() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

}
