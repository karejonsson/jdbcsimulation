package se.prv.jdbcwebservice.sql;

import java.util.HashMap;
import java.util.Map;

import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.utils.GenerateReference;
import se.prv.jdbcsimulering.utils.GenerateSecret;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;

public class TokenUtil {

	public static String getToken(String reference, String username, String jwtsecret, long validity) {
		Map<String, Object> m = new HashMap<>();
		m.put(Constants.reference, reference);
		m.put(Constants.symmetricKey, GenerateSecret.generate());
		return JwtTokenUtil.doGenerateToken(m, username, jwtsecret, validity);
	}

}
