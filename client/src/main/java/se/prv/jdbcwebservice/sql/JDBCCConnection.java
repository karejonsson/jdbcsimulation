package se.prv.jdbcwebservice.sql;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import se.prv.jdbcwebservice.calls.CallsBySignature;
import se.prv.jdbcwebservice.calls.GetHashMapPOST;
import se.prv.jdbcwebservice.calls.GetVoidPOST;
import se.prv.jdbcwebservice.calls.ManagedCall;

public class JDBCCConnection implements Connection {

	private static Logger logg = Logger.getLogger(JDBCCConnection.class); 
	
	private String url_base = null;
	private String url_full = null;
	private String username = null;
	private String jwtsecret = null;
	private long validity;
	private String reference = null;
	
	public JDBCCConnection(String url, String username, String jwtsecret, long validity, String reference) {
		this.url_base = url;
		this.url_full = url+"/Connection";
		this.username = username;
		this.jwtsecret = jwtsecret;
		this.validity = validity;
		this.reference = reference;
	}
	
	public String toString() {
		return "JDBCCConnection{url_full="+url_full+", username="+username+", reference="+reference+"}";
	}

	public String getReference() {
		return reference;
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void abort(Executor arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void clearWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}
	
	@Override
	public void close() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/close");
	}

	@Override
	public void commit() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/commit");
	}

	@Override
	public Array createArrayOf(String arg0, Object[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Blob createBlob() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Clob createClob() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public NClob createNClob() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}
	
	@Override
	public Statement createStatement() throws SQLException {
		String stmtreference = ManagedCall.getReference(url_full+"/createStatement", reference, username, jwtsecret, validity);
		return new JDBCCStatement(url_base, this, stmtreference);
	}

	@Override
	public Statement createStatement(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Statement createStatement(int arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Struct createStruct(String arg0, Object[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getCatalog() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getClientInfo(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String getSchema() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isValid(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public String nativeSQL(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		String stmtreference = ManagedCall.getReference(url_full+"/prepareCall", reference, username, jwtsecret, validity, sql);
		return new JDBCCCallableStatement(url_base, this, stmtreference);
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		String stmtreference = ManagedCall.getReference(url_full+"/prepareStatement", reference, username, jwtsecret, validity, sql);
		return new JDBCCPreparedStatement(url_base, this, stmtreference);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		String stmtreference = ManagedCall.getReference(url_full+"/prepareStatementWithKeys", reference, username, jwtsecret, validity, sql, autoGeneratedKeys);
		return new JDBCCPreparedStatement(url_base, this, stmtreference);
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void releaseSavepoint(Savepoint arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void rollback() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/rollback");
	}

	@Override
	public void rollback(Savepoint arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");	
	}

	// Map<String, Object> GetHashMapPOST.from_string(String url, String token, String stringname, String stringvalue)
	@Override
	public void setAutoCommit(boolean flag) throws SQLException {
		CallsBySignature.boolean_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_full+"/setAutoCommit", "flag", flag);
	}

	@Override
	public void setCatalog(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setClientInfo(Properties arg0) throws SQLClientInfoException {
		throw new SQLClientInfoException("Saknar implementering på klientsidan", null);
	}

	@Override
	public void setClientInfo(String arg0, String arg1) throws SQLClientInfoException {
		throw new SQLClientInfoException("Saknar implementering på klientsidan", null);
	}

	@Override
	public void setHoldability(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setNetworkTimeout(Executor arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");	
	}

	@Override
	public void setReadOnly(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Savepoint setSavepoint(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setSchema(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTransactionIsolation(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	public String getUsername() {
		return username;
	}

	public String getJwtsecret() {
		return jwtsecret;
	}

	public long getValidity() {
		return validity;
	}

}
