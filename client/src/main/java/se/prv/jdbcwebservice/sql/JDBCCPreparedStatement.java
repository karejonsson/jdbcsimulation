package se.prv.jdbcwebservice.sql;

import java.io.InputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;
import se.prv.jdbcwebservice.calls.CallsBySignature;
import se.prv.jdbcwebservice.calls.ManagedCall;

public class JDBCCPreparedStatement extends JDBCCStatement implements PreparedStatement {

	private static Logger logg = Logger.getLogger(JDBCCPreparedStatement.class); 

	private String url_fullps = null;

	public JDBCCPreparedStatement(String url, JDBCCConnection conn, String stmtreference) {
		super(url, conn, stmtreference);
		this.url_fullps = url+"/PreparedStatement";
	}

	public String toString() {
		return "JDBCCPreparedStatement{url_fullps="+url_fullps+", username="+username+", reference="+reference+"}";
	}

	@Override
	public void addBatch(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void cancel() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/cancel");
	}

	@Override
	public void clearBatch() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/clearBatch");
	}

	@Override
	public void clearWarnings() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/clearWarnings");
	}

	@Override
	public void close() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/close");
	}

	@Override
	public void closeOnCompletion() throws SQLException {
		CallsBySignature.void_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/closeOnCompletion");
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		try {
			return CallsBySignature.string_in_boolean_out(
					TokenUtil.getToken(reference, username, jwtsecret, validity),
					url_fullps+"/execute", "sql", SymetricEncryptionUtil.encrypt(sql, jwtsecret));
		}
		catch(Exception e) {
			throw new SQLException(e);
		}
	}

	@Override
	public boolean execute(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean execute(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int[] executeBatch() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		String rsreference;
		try {
			rsreference = ManagedCall.getReference(
					url_fullps+"/executeQuerySql", 
					reference, 
					username, 
					jwtsecret, 
					validity, 
					sql);
		}
		catch(Exception e) {
			if(e instanceof SQLException) {
				throw (SQLException) e;
			}
			throw new SQLException(e);
		}
		return new JDBCCResultSet(url_base, this, rsreference);
	}

	@Override
	public int executeUpdate(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int executeUpdate(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public Connection getConnection() throws SQLException {
		return conn;
	}

	@Override
	public int getFetchDirection() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getFetchSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getMaxRows() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean getMoreResults(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getResultSetType() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public int getUpdateCount() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public boolean isPoolable() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setEscapeProcessing(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setMaxFieldSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setPoolable(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void addBatch() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");	
	}

	@Override
	public void clearParameters() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");	
	}

	@Override
	public boolean execute() throws SQLException {
		return CallsBySignature.void_in_boolean_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/execute");
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		String rsreference = ManagedCall.getReference(url_fullps+"/executeQuery", reference, username, jwtsecret, validity);
		return new JDBCCResultSet(url_base, this, rsreference);
	}

	@Override
	public int executeUpdate() throws SQLException {
		return CallsBySignature.void_in_int_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/executeUpdate");
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");
	}

	@Override
	public void setArray(int arg0, Array arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBlob(int arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBlob(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setByte(int pos, byte val) throws SQLException {
		CallsBySignature.int_byte_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setByte", "pos", pos, "val", val);
	}

	@Override
	public void setBytes(int pos, byte[] val) throws SQLException {
		CallsBySignature.int_byteA_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setByte", "pos", pos, "val", val);
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");	
	}

	@Override
	public void setClob(int arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setDate(int pos, Date date) throws SQLException {
		CallsBySignature.int_date_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setDate", "pos", pos, "valmillis", date);
	}

	@Override
	public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setDouble(int pos, double val) throws SQLException {
		CallsBySignature.int_double_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setDouble", "pos", pos, "val", val);
	}

	@Override
	public void setFloat(int pos, float val) throws SQLException {
		CallsBySignature.int_float_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setFloat", "pos", pos, "val", val);
	}

	@Override
	public void setInt(int pos, int val) throws SQLException {
		CallsBySignature.int_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setInt", "pos", pos, "val", val);
	}

	@Override
	public void setLong(int pos, long val) throws SQLException {
		CallsBySignature.int_long_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setLong", "pos", pos, "val", val);
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setNClob(int arg0, NClob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setNClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setNString(int pos, String val) throws SQLException {
		CallsBySignature.int_string_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setNString", "pos", pos, "val", val);
	}

	@Override
	public void setNull(int pos, int type) throws SQLException {
		CallsBySignature.int_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setNull", "pos", pos, "type", type);
	}

	@Override
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setObject(int pos, Object obj) throws SQLException {
		CallsBySignature.int_int_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setObject", "pos", pos, "obj", obj);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setRef(int arg0, Ref arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setRowId(int arg0, RowId arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setShort(int pos, short val) throws SQLException {
		CallsBySignature.int_short_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setShort", "pos", pos, "val", val);
	}

	@Override
	public void setString(int pos, String val) throws SQLException {
		CallsBySignature.int_string_in_void_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity),
				url_fullps+"/setString", "pos", pos, "val", val);
	}

	@Override
	public void setTime(int arg0, Time arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setURL(int arg0, URL arg1) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

	@Override
	public void setUnicodeStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på klientsidan");		
	}

}
