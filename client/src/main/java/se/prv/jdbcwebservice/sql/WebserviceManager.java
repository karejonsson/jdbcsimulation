package se.prv.jdbcwebservice.sql;

import java.util.HashMap;
import java.util.Map;

import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.utils.GenerateSecret;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;
import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;
import se.prv.jdbcwebservice.calls.GetHashMapPOST;

public class WebserviceManager {

	public static JDBCCConnection getConnection(String url, String username, String jwtsecret, long validity) throws Exception {
		Map<String, Object> a = new HashMap<>();
		String secret = GenerateSecret.generate();
		a.put(Constants.symmetricKey, secret);
		
		String token = JwtTokenUtil.doGenerateToken(a, username, jwtsecret, validity);
		
		Map<String, Object> m = GetHashMapPOST.from_void(url+"/WebserviceManager/getConnection", token);
		if(m == null) {
			throw new Exception("Ogiltigt svar");
		}
		Object obj = m.get(Constants.reference);
		if(obj == null) {
			throw new Exception("Ogiltigt referens");
		}
		Object exc = m.get(Constants.exception);
		if(exc != null) {
			if(!(exc instanceof Exception)) {
				throw new Exception("Ett exception-objekt har returnerats men det är inte ett riktigt objekt");
			}
			throw (Exception) exc;
		}
		
		return new JDBCCConnection(url, username, jwtsecret, validity, SymetricEncryptionUtil.decrypt(obj.toString(), secret));
	}

}
