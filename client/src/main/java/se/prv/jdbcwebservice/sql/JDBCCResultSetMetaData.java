package se.prv.jdbcwebservice.sql;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.Map;

import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcwebservice.calls.CallsBySignature;
import se.prv.jdbcwebservice.calls.GetHashMapPOST;

public class JDBCCResultSetMetaData implements ResultSetMetaData {
	
	private String url_base = null;
	private String url_fullrsmd = null;
	private JDBCCResultSet resultSet = null; 
	private String reference = null;
	protected String username = null;
	protected String jwtsecret = null;
	protected long validity;

	public JDBCCResultSetMetaData(String url_base, JDBCCResultSet resultSet, String reference) {
		this.url_base = url_base;
		this.resultSet = resultSet;
		this.reference = reference;
		this.username = resultSet.getUsername();
		this.jwtsecret = resultSet.getJwtsecret();
		this.validity = resultSet.getValidity();
		url_fullrsmd = url_base+"/ResultSetMetaData";
	}
	
	public String toString() {
		return "JDBCCResultSetMetaData{reference="+reference+"}";
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getColumnCount() throws SQLException {
		return CallsBySignature.void_in_int_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrsmd+"/getColumnCount");
	}

	@Override
	public boolean isAutoIncrement(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCaseSensitive(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSearchable(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isCurrency(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int isNullable(int column) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isSigned(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int getColumnDisplaySize(int column) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getColumnLabel(int column) throws SQLException {
		return CallsBySignature.int_in_string_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrsmd+"/getColumnLabel", "column", column);
	}

	@Override
	public String getColumnName(int column) throws SQLException {
		return CallsBySignature.int_in_string_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrsmd+"/getColumnName", "column", column);
	}

	@Override
	public String getSchemaName(int column) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getPrecision(int column) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getScale(int column) throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getTableName(int column) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCatalogName(int column) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getColumnType(int column) throws SQLException {
		return CallsBySignature.int_in_int_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrsmd+"/getColumnType", "column", column);
	}

	@Override
	public String getColumnTypeName(int column) throws SQLException {
		return CallsBySignature.int_in_string_out(
				TokenUtil.getToken(reference, username, jwtsecret, validity), 
				url_fullrsmd+"/getColumnTypeName", "column", column);
	}

	@Override
	public boolean isReadOnly(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isWritable(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isDefinitelyWritable(int column) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getColumnClassName(int column) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
