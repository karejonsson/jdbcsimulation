package se.prv.jdbcwebservice.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;
import java.sql.Types;
import java.sql.CallableStatement;
import java.sql.Date;

import se.prv.jdbcsimulering.utils.JwtTokenUtil;

public class TestTemplateJDBC {

	public static void main(String[] args) throws Exception {
		JDBCCConnection conn = WebserviceManager.getConnection("http://localhost:8080", "Kåre", JwtTokenUtil.jwtsecret, 15000l);
		System.out.println("Conn "+conn);
		conn.close();
		Statement stmt = conn.createStatement();
		System.out.println("stmt "+stmt);
		stmt.close();
		PreparedStatement ps = conn.prepareStatement("hej gulliga lilla sql");
		System.out.println("ps "+ps);
		ps.setInt(1, 2);
		ps.setDouble(3, 4);
		ps.setFloat(5, 6);
		ps.setLong(7, 8);
		ps.setShort(7, (short)8);
		conn.rollback();
		conn.commit();
		conn.setAutoCommit(true);
		ps.setDate(1, new Date(System.currentTimeMillis()));
		ps.setNull(1, Types.DOUBLE);
		ps = conn.prepareStatement("hej gulliga lilla sql", 7);
		
		System.out.println("---------------------");
		CallableStatement cs = conn.prepareCall("Ett mindre gulligt SQL :-(");
		System.out.println(cs.toString());
		cs.setInt(1, 2);
		cs.setString(5, "Hej sträng");
		cs.setDouble(3, 4);
		cs.setFloat(5, 6);
		cs.setLong(7, 8);
		cs.setShort(7, (short)8);
		cs.setDate(1, new Date(System.currentTimeMillis()));
		cs.setNull(1, Types.DOUBLE);

		cs.setInt("namn", 2);
		cs.setString("namn", "Hej sträng");
		cs.setDouble("namn", 4);
		cs.setFloat("namn", 6);
		cs.setLong("namn", 8);
		cs.setShort("namn", (short)8);
		cs.setDate("namn", new Date(System.currentTimeMillis()));
		cs.setNull("namn", Types.DOUBLE);
		
		System.out.println("---------------------");

		System.out.println("PreparedStatement.execute -> "+ps.execute());
		System.out.println("PreparedStatement.executeUpdate -> "+ps.executeUpdate());
		
		System.out.println("CallableStatement.execute -> "+cs.execute());
		System.out.println("CallableStatement.executeUpdate -> "+cs.executeUpdate());
		
		ResultSet rs = ps.executeQuery();
		for(int i = 0 ; i < 3 ; i++) {
			System.out.println("P: ResultSet.next -> "+rs.next());
			System.out.println("P: ResultSet.getInt -> "+rs.getInt(4));
			System.out.println("P: ResultSet.getInt -> "+rs.getInt("name"));
			System.out.println("P: ResultSet.getShort -> "+rs.getShort(4));
			System.out.println("P: ResultSet.getShort -> "+rs.getShort("name"));
			System.out.println("P: ResultSet.getLong -> "+rs.getLong(4));
			System.out.println("P: ResultSet.getLong -> "+rs.getLong("name"));
			System.out.println("P: ResultSet.getDouble -> "+rs.getDouble(4));
			System.out.println("P: ResultSet.getDouble -> "+rs.getDouble("name"));
			System.out.println("P: ResultSet.getString -> "+rs.getString(4));
			System.out.println("P: ResultSet.getString -> "+rs.getString("name"));
			System.out.println("P: ResultSet.getDate -> "+rs.getDate(4));
			System.out.println("P: ResultSet.getDate -> "+rs.getDate("name"));
			System.out.println("P: ResultSet.getObject -> "+rs.getObject(4));			
			System.out.println("P: ResultSet.getObject -> "+rs.getObject("name"));			
		}

		rs = cs.executeQuery();
		for(int i = 0 ; i < 3 ; i++) {
			System.out.println("C: ResultSet.next -> "+rs.next());
			System.out.println("C: ResultSet.getInt -> "+rs.getInt(4));
			System.out.println("C: ResultSet.getShort -> "+rs.getShort(4));
			System.out.println("C: ResultSet.getLong -> "+rs.getLong(4));
			System.out.println("C: ResultSet.getDouble -> "+rs.getDouble(4));
			System.out.println("C: ResultSet.getString -> "+rs.getString(4));
			System.out.println("C: ResultSet.getDate -> "+rs.getDate(4));
			System.out.println("C: ResultSet.getObject -> "+rs.getObject(4));			

			System.out.println("C: ResultSet.getInt -> "+rs.getInt("name"));
			System.out.println("C: ResultSet.getShort -> "+rs.getShort("name"));
			System.out.println("C: ResultSet.getLong -> "+rs.getLong("name"));
			System.out.println("C: ResultSet.getDouble -> "+rs.getDouble("name"));
			System.out.println("C: ResultSet.getString -> "+rs.getString("name"));
			System.out.println("C: ResultSet.getDate -> "+rs.getDate("name"));
			System.out.println("C: ResultSet.getObject -> "+rs.getObject("name"));			
		}
		
		ResultSetMetaData rsmd = rs.getMetaData();
		System.out.println("ResultSetMetaData -> "+rsmd);	
		System.out.println("ResultSetMetaData.getColumnCount -> "+rsmd.getColumnCount());	
	}

}
