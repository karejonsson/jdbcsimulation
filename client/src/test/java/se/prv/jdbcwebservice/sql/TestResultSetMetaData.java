package se.prv.jdbcwebservice.sql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import se.prv.jdbcsimulering.utils.JwtTokenUtil;

public class TestResultSetMetaData {

	public static void main(String[] args) throws Exception {
		JDBCCConnection conn = WebserviceManager.getConnection("http://localhost:8080", "Kåre", JwtTokenUtil.jwtsecret, 15000l);
		PreparedStatement ps = conn.prepareStatement("hej sql");
		ResultSet rs = ps.executeQuery();
		ResultSetMetaData rsmd = rs.getMetaData();
		System.out.println("rsmd.getColumnCount() -> "+rsmd.getColumnCount());
		System.out.println("rsmd.getColumnLabel(1) -> "+rsmd.getColumnLabel(1));
		System.out.println("rsmd.getColumnName(2) -> "+rsmd.getColumnName(2));
		System.out.println("rsmd.getColumnType(4) -> "+rsmd.getColumnType(4));
		System.out.println("rsmd.getColumnTypeName(5) -> "+rsmd.getColumnTypeName(5));
	}

}
