package se.prv.jdbcsimulering.call;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class PingGet {

	public static String ping_post(String url_endpoint) throws Exception {

		while(url_endpoint.endsWith("/")) {
			url_endpoint = url_endpoint.substring(0, url_endpoint.length()-1);
		}

		String url = url_endpoint+"/test/ping/gegt";///test/ping/post
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("GET");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		//con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = null;
		Integer recievedResponseCode = null;
		
		try {
			wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes("");
			wr.flush();
			wr.close();
			
			recievedResponseCode = con.getResponseCode();
			//System.out.println("\nSending 'POST' request to URL : " + url);
			//System.out.println("Post parameters : " + urlParameters);
			//System.out.println("Response Code : " + recievedResponseCode);
	
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			
			in.close();
			
			return response.toString();
		}
		catch(Exception e) {
		}
		finally {
			try { if(wr != null) { wr.close(); }} catch(Exception e) {}
			try { if(con != null) { con.disconnect(); }} catch(Exception e) {}
		}
		return "FEL";
	}
	
	public static void main(String[] args) throws Exception {
		String m = ping_post("http://localhost:8080/WebserviceManager");
		System.out.println(m);
	}

}
