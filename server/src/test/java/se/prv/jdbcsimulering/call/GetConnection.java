package se.prv.jdbcsimulering.call;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import se.prv.jdbcsimulering.utils.JwtTokenUtil;
import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;

public class GetConnection {
	
	public static final String httpResponseCode = "httpresponsecode";

	public static Map<String, Object> getConnection(String url_endpoint) throws Exception {

		while(url_endpoint.endsWith("/")) {
			url_endpoint = url_endpoint.substring(0, url_endpoint.length()-1);
		}
		String url = url_endpoint+"/WebserviceManager/getConnection";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		String basicAuth = JwtTokenUtil.generateToken("Kåre utvecklar", JwtTokenUtil.jwtsecret, 180000l);
		con.setRequestProperty("Authorization", basicAuth);
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		Integer recievedResponseCode = null;
		
		try {
			recievedResponseCode = con.getResponseCode();
			if (recievedResponseCode != 200) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ con.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
				(con.getInputStream())));

			StringBuffer response = new StringBuffer();
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				response.append(output);
			}
			System.out.println("-> "+response.toString());
			TypeReference<HashMap<String, Object>> typeRef = new TypeReference<HashMap<String, Object>>() {};
			
			ObjectMapper mapper = new ObjectMapper();
			Map<String, Object> map = mapper.readValue(response.toString(), typeRef);
			return map;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try { if(con != null) { con.disconnect(); }} catch(Exception e) {}
		}
		Map<String, Object> map = new HashMap<>();
		map.put(httpResponseCode, new Integer(recievedResponseCode != null ? recievedResponseCode : 500));
		return map;
	}
	
	public static void main(String[] args) throws Exception {
		Map<String, Object> m = getConnection("http://localhost:8080");
		for(String key : m.keySet()) {
			System.out.println("Key "+key+", value "+m.get(key));
		}
		String ref = (String )m.get("reference");
		System.out.println("Referens i klartext <"+SymetricEncryptionUtil.decrypt(ref, "secret..")+">");
		
	}

}
