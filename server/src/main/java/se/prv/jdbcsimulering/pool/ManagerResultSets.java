package se.prv.jdbcsimulering.pool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.serverobjects.JDBCSConnection;
import se.prv.jdbcsimulering.serverobjects.JDBCSPreparedStatement;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSet;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSetMetaData;
import se.prv.jdbcsimulering.serverobjects.JDBCSStatement;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;

public class ManagerResultSets {

	private static Logger logg = Logger.getLogger(ManagerResultSets.class); 

	private static Map<String, JDBCSResultSet> hash = new HashMap<>();

	public static JDBCSResultSet newResultSet_executeQuery(JDBCSPreparedStatement stmt) throws SQLException {
		ResultSet rs = stmt.getPreparedStatement().executeQuery();
		JDBCSResultSet out = new JDBCSResultSet(stmt, rs, stmt.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static JDBCSResultSet lookup(String reference) {
		return hash.get(reference);
	}
	
	public static JDBCSResultSet lookup(Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		return lookup(he);
	}

	public static JDBCSResultSet lookup(HeaderExtract he) {
		logg.info("extract="+he);
	    if(he.reference != null) {
	    	return lookup(he.reference);
	    }
	    return null;
	}

	public static ResultSet newResultSet_getResultSet(JDBCSPreparedStatement stmt) throws SQLException {
		ResultSet rs = stmt.getPreparedStatement().getResultSet();
		JDBCSResultSet out = new JDBCSResultSet(stmt, rs, stmt.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static ResultSet newResultSet_getResultSet(JDBCSStatement stmt) throws SQLException {
		ResultSet rs = stmt.getStatement().getResultSet();
		JDBCSResultSet out = new JDBCSResultSet(stmt, rs, stmt.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static ResultSet newResultSet_getGeneratedKeys(JDBCSPreparedStatement stmt) throws SQLException {
		ResultSet rs = stmt.getPreparedStatement().getGeneratedKeys();
		JDBCSResultSet out = new JDBCSResultSet(stmt, rs, stmt.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static ResultSet newResultSet_getGeneratedKeys(JDBCSStatement stmt) throws SQLException {
		ResultSet rs = stmt.getStatement().getGeneratedKeys();
		JDBCSResultSet out = new JDBCSResultSet(stmt, rs, stmt.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static ResultSet newResultSet_executeQuery(JDBCSPreparedStatement stmt, String sql) throws SQLException {
		ResultSet rs = stmt.getPreparedStatement().executeQuery(sql);
		JDBCSResultSet out = new JDBCSResultSet(stmt, rs, stmt.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	static void purge() {
		List<String> remove = new ArrayList<>();
		for(JDBCSResultSet rs : hash.values()) {
			if(rs.isExpired()) {
				remove.add(rs.getReference());
			}
		}
		for(String ref : remove) {
			JDBCSResultSet rs = hash.get(ref);
			logg.info("Rensar bort ett ResultSet-objekt. rs="+rs);
			hash.remove(rs.getReference());
		}
	}

}
