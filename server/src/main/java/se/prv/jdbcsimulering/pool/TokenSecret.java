package se.prv.jdbcsimulering.pool;

import se.prv.jdbcsimulering.utils.JwtTokenUtil;

public class TokenSecret {
	
	public static String jwtsecret = null;
	
	static {
		jwtsecret = JwtTokenUtil.jwtsecret;
	}

}
