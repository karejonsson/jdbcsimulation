package se.prv.jdbcsimulering.pool;

import java.sql.ParameterMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.serverobjects.JDBCSParameterMetaData;
import se.prv.jdbcsimulering.serverobjects.JDBCSPreparedStatement;

public class ManagerParameterMetaDatas {
	
	private static Logger logg = Logger.getLogger(ManagerParameterMetaDatas.class); 
	
	private static Map<String, JDBCSParameterMetaData> hash = new HashMap<>();

	public static ParameterMetaData newParameterMetaData(JDBCSPreparedStatement ps) throws SQLException {
		JDBCSParameterMetaData out = new JDBCSParameterMetaData(ps.getPreparedStatement().getParameterMetaData(), ps.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	static void purge() {
		List<String> remove = new ArrayList<>();
		for(JDBCSParameterMetaData rsmd : hash.values()) {
			if(rsmd.isExpired()) {
				remove.add(rsmd.getReference());
			}
		}
		for(String ref : remove) {
			JDBCSParameterMetaData rsmd = hash.get(ref);
			logg.info("Rensar bort ett JDBCSResultSetMetaData-objekt. rsmd="+rsmd);
			hash.remove(rsmd.getReference());
		}
	}


}

