package se.prv.jdbcsimulering.pool;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.serverobjects.JDBCSConnection;
import se.prv.jdbcsimulering.serverobjects.JDBCSPreparedStatement;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSet;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSetMetaData;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;

public class ManagerResultSetMetaDatas {

	private static Logger logg = Logger.getLogger(ManagerResultSetMetaDatas.class); 

	private static Map<String, JDBCSResultSetMetaData> hash = new HashMap<>();

	public static JDBCSResultSetMetaData newResultSetMetaData(JDBCSResultSet rs) throws SQLException {
		JDBCSResultSetMetaData out = new JDBCSResultSetMetaData(rs.getResultSet().getMetaData(), rs.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static ResultSetMetaData newResultSetMetaData(JDBCSPreparedStatement ps) throws SQLException {
		JDBCSResultSetMetaData out = new JDBCSResultSetMetaData(ps.getPreparedStatement().getMetaData(), ps.getValidity());
		hash.put(out.getReference(), out);
		return out;
	}

	public static JDBCSResultSetMetaData lookup(String reference) {
		return hash.get(reference);
	}
	
	public static JDBCSResultSetMetaData lookup(Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		return lookup(he);
	}

	public static JDBCSResultSetMetaData lookup(HeaderExtract he) {
		logg.info("extract="+he);
	    if(he.reference != null) {
	    	return lookup(he.reference);
	    }
	    return null;
	}

	static void purge() {
		List<String> remove = new ArrayList<>();
		for(JDBCSResultSetMetaData rsmd : hash.values()) {
			if(rsmd.isExpired()) {
				remove.add(rsmd.getReference());
			}
		}
		for(String ref : remove) {
			JDBCSResultSetMetaData rsmd = hash.get(ref);
			logg.info("Rensar bort ett JDBCSResultSetMetaData-objekt. rsmd="+rsmd);
			hash.remove(rsmd.getReference());
		}
	}

}
