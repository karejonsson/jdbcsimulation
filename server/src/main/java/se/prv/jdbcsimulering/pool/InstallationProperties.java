package se.prv.jdbcsimulering.pool;

import general.reuse.properties.HarddriveProperties;

public class InstallationProperties {

	private static final String pathToConfigFile_property = "prv.webservice.configfile";
	private static final String pathToConfigFile_default = "/etc/prvconfig/webservice.properties";

	private static HarddriveProperties hp = new HarddriveProperties(pathToConfigFile_property, pathToConfigFile_default);
	 
	public static void setHarddriveProperties(HarddriveProperties hp) {
		InstallationProperties.hp = hp;
	}
	 
	public static HarddriveProperties getHarddriveProperties() {
		return hp;
	}
	 
	public static Integer getInt(String propertyname) throws Exception {
		return hp.getInt(propertyname);
	}

	public static Long getLong(String propertyname) throws Exception {
		return hp.getLong(propertyname);
	}

	public static Long getLong(String propertyname, Long _default) {
		return hp.getLong(propertyname, _default);
	}

	public static Long[] getLongArray(String propertyname) throws Exception {
		return hp.getLongArray(propertyname);
	}

	public static String getString(String propertyname) throws Exception {
		return hp.getString(propertyname);
	}

	public static String[] getStringArray(String propertyname) throws Exception {
		return hp.getStringArray(propertyname);
	}

	public static boolean getBoolean(String propertyname, final boolean defaultValue) {
		return hp.getBoolean(propertyname, defaultValue);
	}
	
	public static String getString(String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}
	
	public static String getString(HarddriveProperties hp, String propertyname, final String defaultValue) {
		return hp.getString(propertyname, defaultValue);
	}
	
	public static Integer getInt(String propertyname, final Integer defaultValue) {
		return hp.getCLPProperty(propertyname, defaultValue);
	}
	
	public static final String jdbc_driver_class_property = "prv.webservice.jdbc.driverclass";
	public static final String jdbc_url_property = "prv.webservice.jdbc.url";
	public static final String jdbc_username_property = "prv.webservice.jdbc.username";
	public static final String jdbc_password_property = "prv.webservice.jdbc.password";
	
	public static final String purgeFrequency = "prv.webservice.purge.frequency";
	public static final String validity = "prv.webservice.validity";

}
