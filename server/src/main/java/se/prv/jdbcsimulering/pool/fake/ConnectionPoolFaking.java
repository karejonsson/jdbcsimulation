package se.prv.jdbcsimulering.pool.fake;

import java.sql.Connection;
import java.sql.SQLException;

import general.reuse.database.pool.JDBCConnectionPool;

public class ConnectionPoolFaking implements JDBCConnectionPool {
	
	public ConnectionPoolFaking(String d, String u, String us, String p) throws SQLException {
	}

	@Override
	public Connection reserveConnection() throws SQLException {
		return new ConnectionFaking();
	}

	@Override
	public void releaseConnection(Connection conn) {
	}

	@Override
	public void destroy() {
	}

}
