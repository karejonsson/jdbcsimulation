package se.prv.jdbcsimulering.pool;

import java.util.Timer;
import java.util.TimerTask;

import org.apache.log4j.Logger;

public class Purger {
	
	private static Logger logg = Logger.getLogger(Purger.class); 
	
	private static boolean started = false;
	
	public static void start() {
		if(started) {
			return;
		}
		started = true;
		TimerTask repeatedTask = new TimerTask() {
	        public void run() {
	        	purge();
	        }
	    };
	    Timer timer = new Timer("Timer");
	     
	    long period = InstallationProperties.getLong(InstallationProperties.purgeFrequency, 300000l);
	    logg.info("prv.webservice.purge.frequency="+period);
	    timer.scheduleAtFixedRate(repeatedTask, period, period);
	}

	private static void purge() {
	    //logg.info("Rensningsjobbet----------");
	    try { ManagerConnections.purge(); } catch(Exception e) { e.printStackTrace(); }
	    try { ManagerResultSetMetaDatas.purge(); } catch(Exception e) { e.printStackTrace(); }
	    try { ManagerResultSets.purge(); } catch(Exception e) { e.printStackTrace(); }
	    try { ManagerStatements.purge(); } catch(Exception e) { e.printStackTrace(); }
	    try { ManagerParameterMetaDatas.purge(); } catch(Exception e) { e.printStackTrace(); }
	}
	
}
