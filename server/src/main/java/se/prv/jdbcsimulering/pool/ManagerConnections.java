package se.prv.jdbcsimulering.pool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import general.reuse.database.pool.JDBCConnectionPool;
import se.prv.jdbcsimulering.pool.fake.ConnectionPoolFaking;
import se.prv.jdbcsimulering.serverobjects.JDBCSConnection;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;
import se.prv.jdbcsimulering.utils.GenerateReference;

public class ManagerConnections {
	
	private static Logger logg = Logger.getLogger(ManagerConnections.class); 
	
	public static JDBCConnectionPool pool = null;
	private static long validity = 0l;

	static {
		try {
			pool = new ConnectionPoolFaking(
					InstallationProperties.getString(InstallationProperties.jdbc_driver_class_property, ""),
					InstallationProperties.getString(InstallationProperties.jdbc_url_property, ""),
					InstallationProperties.getString(InstallationProperties.jdbc_username_property, ""),
					InstallationProperties.getString(InstallationProperties.jdbc_password_property, "")
					);
			validity = InstallationProperties.getLong(InstallationProperties.validity, 300000l);
		}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private static Map<String, JDBCSConnection> hash = new HashMap<>();
	
	public static JDBCSConnection newConnection() throws SQLException {
		Connection conn = pool.reserveConnection();
		JDBCSConnection out = new JDBCSConnection(GenerateReference.generate(), conn, validity);
		hash.put(out.getReference(), out);
		return out;
	}
	
	public static JDBCSConnection lookup(String reference) {
		return hash.get(reference);
	}
	
	public static JDBCSConnection lookup(Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		return lookup(he);
	}

	public static JDBCSConnection lookup(HeaderExtract he) {
	    if(he.reference != null) {
	    	return lookup(he.reference);
	    }
	    return null;
	}
	
	static void purge() {
		List<String> remove = new ArrayList<>();
		for(JDBCSConnection conn : hash.values()) {
			if(conn.isExpired()) {
				remove.add(conn.getReference());
			}
		}
		for(String ref : remove) {
			JDBCSConnection conn = hash.get(ref);
			try {
				logg.info("Rensar bort ett connection-objekt. conn="+conn);
				conn.close();
			}
			catch (SQLException e) {
				logg.error(e);
			}
			hash.remove(conn.getReference());
		}
	}

}
