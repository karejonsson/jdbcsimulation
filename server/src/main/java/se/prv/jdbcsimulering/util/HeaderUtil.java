package se.prv.jdbcsimulering.util;

import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.pool.TokenSecret;
import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;

public class HeaderUtil {
	
	private static Logger logg = Logger.getLogger(HeaderUtil.class); 

	public static HeaderExtract getExtract(Map<String, String> headers) {
		HeaderExtract out = new HeaderExtract();
	    for(String key : headers.keySet()) {
	    	Object value = headers.get(key);
    		//logg.info("HEADER: Key "+key+", value "+value);
	    	if(key.equals("authorization")) {
	    		out.token = headers.get(key);
	    	}
	    }
	    if(out.token != null) {
	    	out.caller = JwtTokenUtil.getCallersNameFromToken(out.token, TokenSecret.jwtsecret);
	    	out.expired = JwtTokenUtil.isTokenExpired(out.token, TokenSecret.jwtsecret);
	    	out.claims = JwtTokenUtil.getAllClaimsFromToken(out.token, TokenSecret.jwtsecret);
		    for(String key : out.claims.keySet()) {
		    	Object value = out.claims.get(key);
	    		//logg.info("TOKEN: Key "+key+", value "+value);
	    		if(key.equals(Constants.symmetricKey)) {
	    			out.secret = value.toString();
	    		}
	    		if(key.equals(Constants.reference)) {
	    			out.reference = value.toString();
	    		}
		    }
	    }
		return out;
	}

}
