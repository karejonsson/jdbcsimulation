package se.prv.jdbcsimulering.util;

import java.util.Map;

public class HeaderExtract {

	public String caller;
	public boolean expired;
	public Map<String, Object> claims;
	public String reference;
	public String secret;
	public String token;

	public String toString() {
		return "HeaderExtract{caller="+caller+", expired="+expired+", reference="+reference+", secret="+secret+"}";
	}
	
}
