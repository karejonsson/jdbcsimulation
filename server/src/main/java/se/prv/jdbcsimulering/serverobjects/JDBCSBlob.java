package se.prv.jdbcsimulering.serverobjects;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Blob;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSBlob implements Blob {
	
	private static Logger logg = Logger.getLogger(JDBCSBlob.class); 

	private Blob blob = null;
	private String reference = null;
	private long validity = 0l;
	protected long expires = 0l;

	public JDBCSBlob(Blob blob, long validity) {
		this.validity = validity;
		this.blob = blob;
		this.reference = GenerateReference.generate();
		expires = System.currentTimeMillis()+validity;
	}
	
	public String getReference() {
		return reference;
	}
	
	public long getValidity() {
		return validity;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}

	@Override
	public void free() throws SQLException {
		updateExpires();
		blob.free();
	}

	@Override
	public InputStream getBinaryStream() throws SQLException {
		updateExpires();
		return blob.getBinaryStream();
	}

	@Override
	public InputStream getBinaryStream(long pos, long length) throws SQLException {
		updateExpires();
		return blob.getBinaryStream(pos, length);
	}

	@Override
	public byte[] getBytes(long pos, int length) throws SQLException {
		updateExpires();
		return blob.getBytes(pos, length);
	}

	@Override
	public long length() throws SQLException {
		updateExpires();
		return blob.length();
	}

	@Override
	public long position(byte[] pattern, long start) throws SQLException {
		updateExpires();
		return blob.position(pattern, start);
	}

	@Override
	public long position(Blob pattern, long start) throws SQLException {
		updateExpires();
		return blob.position(pattern, start);
	}

	@Override
	public OutputStream setBinaryStream(long pos) throws SQLException {
		updateExpires();
		return blob.setBinaryStream(pos);
	}

	@Override
	public int setBytes(long pos, byte[] bytes) throws SQLException {
		updateExpires();
		return blob.setBytes(pos, bytes);
	}

	@Override
	public int setBytes(long pos, byte[] bytes, int offset, int len) throws SQLException {
		updateExpires();
		return blob.setBytes(pos, bytes, offset, len);
	}

	@Override
	public void truncate(long len) throws SQLException {
		updateExpires();
		blob.truncate(len);
	}

}
