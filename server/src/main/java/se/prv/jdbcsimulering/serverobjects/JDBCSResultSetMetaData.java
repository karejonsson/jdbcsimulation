package se.prv.jdbcsimulering.serverobjects;

import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSResultSetMetaData implements ResultSetMetaData {

	private static Logger logg = Logger.getLogger(JDBCSResultSetMetaData.class); 
	
	private ResultSetMetaData rsmd = null;
	private long validity = 0l;
	private String reference = null;
	private long expires = 0l;

	public JDBCSResultSetMetaData(ResultSetMetaData rsmd, long validity) {
		this.rsmd = rsmd;
		this.reference = GenerateReference.generate();
		this.validity = validity;
		updateExpires();
	}

	public String getReference() {
		return reference;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		updateExpires();
		return rsmd.isWrapperFor(iface);
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		updateExpires();
		return rsmd.unwrap(iface);
	}

	@Override
	public String getCatalogName(int column) throws SQLException {
		updateExpires();
		return rsmd.getCatalogName(column);
	}

	@Override
	public String getColumnClassName(int column) throws SQLException {
		updateExpires();
		return rsmd.getColumnClassName(column);
	}

	@Override
	public int getColumnCount() throws SQLException {
		updateExpires();
		return rsmd.getColumnCount();
	}

	@Override
	public int getColumnDisplaySize(int column) throws SQLException {
		updateExpires();
		return rsmd.getColumnDisplaySize(column);
	}

	@Override
	public String getColumnLabel(int column) throws SQLException {
		updateExpires();
		return rsmd.getColumnLabel(column);
	}

	@Override
	public String getColumnName(int column) throws SQLException {
		updateExpires();
		return rsmd.getColumnName(column);
	}

	@Override
	public int getColumnType(int column) throws SQLException {
		updateExpires();
		return rsmd.getColumnType(column);
	}

	@Override
	public String getColumnTypeName(int column) throws SQLException {
		updateExpires();
		return rsmd.getColumnTypeName(column);
	}

	@Override
	public int getPrecision(int column) throws SQLException {
		updateExpires();
		return rsmd.getPrecision(column);
	}

	@Override
	public int getScale(int column) throws SQLException {
		updateExpires();
		return rsmd.getScale(column);
	}

	@Override
	public String getSchemaName(int column) throws SQLException {
		updateExpires();
		return rsmd.getSchemaName(column);
	}

	@Override
	public String getTableName(int column) throws SQLException {
		updateExpires();
		return rsmd.getTableName(column);
	}

	@Override
	public boolean isAutoIncrement(int column) throws SQLException {
		updateExpires();
		return rsmd.isAutoIncrement(column);
	}

	@Override
	public boolean isCaseSensitive(int column) throws SQLException {
		updateExpires();
		return rsmd.isCaseSensitive(column);
	}

	@Override
	public boolean isCurrency(int column) throws SQLException {
		updateExpires();
		return rsmd.isCurrency(column);
	}

	@Override
	public boolean isDefinitelyWritable(int column) throws SQLException {
		updateExpires();
		return rsmd.isDefinitelyWritable(column);
	}

	@Override
	public int isNullable(int column) throws SQLException {
		updateExpires();
		return rsmd.isNullable(column);
	}

	@Override
	public boolean isReadOnly(int column) throws SQLException {
		updateExpires();
		return rsmd.isReadOnly(column);
	}

	@Override
	public boolean isSearchable(int column) throws SQLException {
		updateExpires();
		return rsmd.isSearchable(column);
	}

	@Override
	public boolean isSigned(int column) throws SQLException {
		updateExpires();
		return rsmd.isSigned(column);
	}

	@Override
	public boolean isWritable(int column) throws SQLException {
		updateExpires();
		return rsmd.isWritable(column);
	}

}
