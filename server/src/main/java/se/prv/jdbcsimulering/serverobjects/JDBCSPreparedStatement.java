package se.prv.jdbcsimulering.serverobjects;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.pool.ManagerParameterMetaDatas;
import se.prv.jdbcsimulering.pool.ManagerResultSetMetaDatas;
import se.prv.jdbcsimulering.pool.ManagerResultSets;

public class JDBCSPreparedStatement extends JDBCSStatement implements PreparedStatement {

	private static Logger logg = Logger.getLogger(JDBCSPreparedStatement.class); 
	
	public JDBCSPreparedStatement(JDBCSConnection conn, PreparedStatement stmt, long validity) {
		super(conn, stmt, validity);
	}
	
	@Override
	public void addBatch(String sql) throws SQLException {
		updateExpires();
		stmt.addBatch(sql);
	}

	@Override
	public void cancel() throws SQLException {
		updateExpires();
		stmt.cancel();
	}

	@Override
	public void clearBatch() throws SQLException {
		updateExpires();
		stmt.clearBatch();
	}

	@Override
	public void clearWarnings() throws SQLException {
		updateExpires();
		stmt.clearWarnings();
	}

	@Override
	public void close() throws SQLException {
		updateExpires();
		stmt.close();	
	}

	@Override
	public void closeOnCompletion() throws SQLException {
		updateExpires();
		stmt.closeOnCompletion();
	}

	@Override
	public boolean execute(String sql) throws SQLException {
		updateExpires();
		return stmt.execute(sql);
	}

	@Override
	public boolean execute(String sql, int autoGeneratedKeys) throws SQLException {
		updateExpires();
		return stmt.execute(sql, autoGeneratedKeys);
	}

	@Override
	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		updateExpires();
		return stmt.execute(sql, columnIndexes);
	}

	@Override
	public boolean execute(String sql, String[] columnNames) throws SQLException {
		updateExpires();
		return stmt.execute(sql, columnNames);
	}

	@Override
	public int[] executeBatch() throws SQLException {
		updateExpires();
		return stmt.executeBatch();
	}

	@Override
	public ResultSet executeQuery(String sql) throws SQLException {
		return ManagerResultSets.newResultSet_executeQuery(this, sql);
	}

	@Override
	public int executeUpdate(String sql) throws SQLException {
		updateExpires();
		return stmt.executeUpdate(sql);
	}

	@Override
	public int executeUpdate(String sql, int autoGeneratedKeys) throws SQLException {
		updateExpires();
		return stmt.executeUpdate(sql, autoGeneratedKeys);
	}

	@Override
	public int executeUpdate(String sql, int[] columnIndexes) throws SQLException {
		updateExpires();
		return stmt.executeUpdate(sql, columnIndexes);
	}

	@Override
	public int executeUpdate(String sql, String[] columnNames) throws SQLException {
		updateExpires();
		return stmt.executeUpdate(sql, columnNames);
	}

	@Override
	public Connection getConnection() throws SQLException {
		updateExpires();
		return conn;
	}

	@Override
	public int getFetchDirection() throws SQLException {
		updateExpires();
		return stmt.getFetchDirection();
	}

	@Override
	public int getFetchSize() throws SQLException {
		updateExpires();
		return stmt.getFetchSize();
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		return ManagerResultSets.newResultSet_getGeneratedKeys(this);
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		updateExpires();
		return stmt.getMaxFieldSize();
	}

	@Override
	public int getMaxRows() throws SQLException {
		updateExpires();
		return stmt.getMaxRows();
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		updateExpires();
		return stmt.getMoreResults();
	}

	@Override
	public boolean getMoreResults(int current) throws SQLException {
		updateExpires();
		return stmt.getMoreResults(current);
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		updateExpires();
		return stmt.getQueryTimeout();
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		return ManagerResultSets.newResultSet_getResultSet(this);
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		updateExpires();
		return stmt.getResultSetConcurrency();
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		updateExpires();
		return stmt.getResultSetHoldability();
	}

	@Override
	public int getResultSetType() throws SQLException {
		updateExpires();
		return stmt.getResultSetType();
	}

	@Override
	public int getUpdateCount() throws SQLException {
		updateExpires();
		return stmt.getUpdateCount();
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		updateExpires();
		return stmt.getWarnings();
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		updateExpires();
		return stmt.isCloseOnCompletion();
	}

	@Override
	public boolean isClosed() throws SQLException {
		updateExpires();
		return stmt.isClosed();
	}

	@Override
	public boolean isPoolable() throws SQLException {
		updateExpires();
		return stmt.isPoolable();
	}

	@Override
	public void setCursorName(String name) throws SQLException {
		updateExpires();
		stmt.setCursorName(name);
	}

	@Override
	public void setEscapeProcessing(boolean enable) throws SQLException {
		updateExpires();
		stmt.setEscapeProcessing(enable);
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
		updateExpires();
		stmt.setFetchDirection(direction);
	}

	@Override
	public void setFetchSize(int rows) throws SQLException {
		updateExpires();
		stmt.setFetchSize(rows);
	}

	@Override
	public void setMaxFieldSize(int max) throws SQLException {
		updateExpires();
		stmt.setMaxFieldSize(max);
	}

	@Override
	public void setMaxRows(int max) throws SQLException {
		updateExpires();
		stmt.setMaxRows(max);
	}

	@Override
	public void setPoolable(boolean poolable) throws SQLException {
		updateExpires();
		stmt.setPoolable(poolable);
	}

	@Override
	public void setQueryTimeout(int seconds) throws SQLException {
		updateExpires();
		stmt.setQueryTimeout(seconds);
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		updateExpires();
		return stmt.isWrapperFor(iface);
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		updateExpires();
		return stmt.unwrap(iface);
	}

	@Override
	public void addBatch() throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).addBatch();
	}

	@Override
	public void clearParameters() throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).clearParameters();
	}

	@Override
	public boolean execute() throws SQLException {
		return ((PreparedStatement) stmt).execute();
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		return ManagerResultSets.newResultSet_executeQuery(this);
	}

	@Override
	public int executeUpdate() throws SQLException {
		return ((PreparedStatement) stmt).executeUpdate();
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		updateExpires();
		return ManagerResultSetMetaDatas.newResultSetMetaData(this);
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		updateExpires();
		return ManagerParameterMetaDatas.newParameterMetaData(this);
	}

	@Override
	public void setArray(int arg0, Array arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBlob(int arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");	
	}

	@Override
	public void setBlob(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setByte(int pos, byte val) throws SQLException {
		((PreparedStatement) stmt).setByte(pos, val);
	}

	@Override
	public void setBytes(int pos, byte[] val) throws SQLException {
		((PreparedStatement) stmt).setBytes(pos, val);
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");	
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setClob(int arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setDate(int pos, Date val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setDate(pos, val);
	}

	@Override
	public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setDouble(int pos, double val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setDouble(pos, val);
	}

	@Override
	public void setFloat(int pos, float val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setFloat(pos, val);
	}

	@Override
	public void setInt(int pos, int val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setInt(pos, val);
	}

	@Override
	public void setLong(int pos, long val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setLong(pos, val);
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setNClob(int arg0, NClob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setNClob(int arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setNString(int pos, String val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setNString(pos, val);
	}

	@Override
	public void setNull(int pos, int type) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setNull(pos, type);
	}

	@Override
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setObject(int pos, Object obj) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setObject(pos, obj);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setRef(int arg0, Ref arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setRowId(int arg0, RowId arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setShort(int pos, short val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setShort(pos, val);
	}

	@Override
	public void setString(int pos, String val) throws SQLException {
		updateExpires();
		((PreparedStatement) stmt).setString(pos, val);
	}

	@Override
	public void setTime(int arg0, Time arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setURL(int arg0, URL arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setUnicodeStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	public PreparedStatement getPreparedStatement() {
		updateExpires();
		return (PreparedStatement) stmt;
	}

}
