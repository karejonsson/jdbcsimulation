package se.prv.jdbcsimulering.serverobjects;

import java.sql.RowId;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSRowId implements RowId {

	private static Logger logg = Logger.getLogger(JDBCSRowId.class); 
	
	private String reference = null;
	protected JDBCSStatement stmt = null;
	private RowId ri = null;
	private long validity = 0l;
	private long expires = 0l;

	public JDBCSRowId(JDBCSStatement stmt, RowId ri, long validity) {
		this.stmt = stmt;
		this.ri = ri;
		this.validity = validity;
		this.reference = GenerateReference.generate();
		updateExpires();
	}
	
	public long getValidity() {
		return validity;
	}

	public String getReference() {
		return reference;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}
	
	@Override
	public byte[] getBytes() {
		updateExpires();
		return ri.getBytes();
	}

}
