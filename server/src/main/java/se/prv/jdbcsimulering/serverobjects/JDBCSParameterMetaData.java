package se.prv.jdbcsimulering.serverobjects;

import java.sql.Clob;
import java.sql.ParameterMetaData;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSParameterMetaData implements ParameterMetaData {

	private static Logger logg = Logger.getLogger(JDBCSParameterMetaData.class); 

	private ParameterMetaData pmd = null;
	private String reference = null;
	private long validity = 0l;
	protected long expires = 0l;

	public JDBCSParameterMetaData(ParameterMetaData pmd, long validity) {
		this.validity = validity;
		this.pmd = pmd;
		this.reference = GenerateReference.generate();
		expires = System.currentTimeMillis()+validity;
	}

	public String getReference() {
		return reference;
	}
	
	public long getValidity() {
		return validity;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		updateExpires();
		return pmd.isWrapperFor(iface);
	}

	@Override
	public <T> T unwrap(Class<T> iface) throws SQLException {
		updateExpires();
		return pmd.unwrap(iface);
	}

	@Override
	public String getParameterClassName(int param) throws SQLException {
		updateExpires();
		return pmd.getParameterClassName(param);
	}

	@Override
	public int getParameterCount() throws SQLException {
		updateExpires();
		return pmd.getParameterCount();
	}

	@Override
	public int getParameterMode(int param) throws SQLException {
		updateExpires();
		return pmd.getParameterMode(param);
	}

	@Override
	public int getParameterType(int param) throws SQLException {
		updateExpires();
		return pmd.getParameterType(param);
	}

	@Override
	public String getParameterTypeName(int param) throws SQLException {
		updateExpires();
		return pmd.getParameterTypeName(param);
	}

	@Override
	public int getPrecision(int param) throws SQLException {
		updateExpires();
		return pmd.getPrecision(param);
	}

	@Override
	public int getScale(int param) throws SQLException {
		updateExpires();
		return pmd.getScale(param);
	}

	@Override
	public int isNullable(int param) throws SQLException {
		updateExpires();
		return pmd.isNullable(param);
	}

	@Override
	public boolean isSigned(int param) throws SQLException {
		updateExpires();
		return pmd.isSigned(param);
	}

}
