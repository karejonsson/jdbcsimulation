package se.prv.jdbcsimulering.serverobjects;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Date;
import java.sql.NClob;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Statement;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.pool.ManagerResultSetMetaDatas;
import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSResultSet implements ResultSet {

	private static Logger logg = Logger.getLogger(JDBCSResultSet.class); 
	
	private String reference = null;
	protected JDBCSStatement stmt = null;
	private ResultSet rs = null;
	private long validity = 0l;
	private long expires = 0l;

	public JDBCSResultSet(JDBCSStatement stmt, ResultSet rs, long validity) {
		this.stmt = stmt;
		this.rs = rs;
		this.validity = validity;
		this.reference = GenerateReference.generate();
		updateExpires();
	}
	
	public long getValidity() {
		return validity;
	}

	public String getReference() {
		return reference;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}
	
	public ResultSet getResultSet() {
		return rs;
	}

	@Override
	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return rs.isWrapperFor(iface);
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean absolute(int row) throws SQLException {
		updateExpires();
		return rs.absolute(row);
	}

	@Override
	public void afterLast() throws SQLException {
		updateExpires();
		rs.afterLast();		
	}

	@Override
	public void beforeFirst() throws SQLException {
		updateExpires();
		rs.beforeFirst();
	}

	@Override
	public void cancelRowUpdates() throws SQLException {
		updateExpires();
		rs.cancelRowUpdates();
	}

	@Override
	public void clearWarnings() throws SQLException {
		updateExpires();
		rs.clearWarnings();	
	}

	@Override
	public void close() throws SQLException {
		updateExpires();
		rs.close();		
	}

	@Override
	public void deleteRow() throws SQLException {
		updateExpires();
		rs.deleteRow();		
	}

	@Override
	public int findColumn(String columnLabel) throws SQLException {
		updateExpires();
		return rs.findColumn(columnLabel);
	}

	@Override
	public boolean first() throws SQLException {
		updateExpires();
		return rs.first();
	}

	@Override
	public Array getArray(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getArray(columnIndex);
	}

	@Override
	public Array getArray(String columnIndex) throws SQLException {
		updateExpires();
		return rs.getArray(columnIndex);
	}

	@Override
	public InputStream getAsciiStream(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getAsciiStream(columnIndex);
	}

	@Override
	public InputStream getAsciiStream(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getAsciiStream(columnLabel);
	}

	@Override
	public BigDecimal getBigDecimal(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getBigDecimal(columnIndex);
	}

	@Override
	public BigDecimal getBigDecimal(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getBigDecimal(columnLabel);
	}

	@Override
	public BigDecimal getBigDecimal(int columnIndex, int scale) throws SQLException {
		updateExpires();
		return rs.getBigDecimal(columnIndex, scale);
	}

	@Override
	public BigDecimal getBigDecimal(String columnLabel, int scale) throws SQLException {
		updateExpires();
		return rs.getBigDecimal(columnLabel, scale);
	}

	@Override
	public InputStream getBinaryStream(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getBinaryStream(columnIndex);
	}
		
	@Override
	public InputStream getBinaryStream(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getBinaryStream(columnLabel);
	}

	@Override
	public Blob getBlob(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getBlob(columnIndex);
	}

	@Override
	public Blob getBlob(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getBlob(columnLabel);
	}

	@Override
	public boolean getBoolean(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getBoolean(columnIndex);
	}

	@Override
	public boolean getBoolean(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getBoolean(columnLabel);
	}

	@Override
	public byte getByte(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getByte(columnIndex);
	}

	@Override
	public byte getByte(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getByte(columnLabel);
	}

	@Override
	public byte[] getBytes(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getBytes(columnIndex);
	}

	@Override
	public byte[] getBytes(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getBytes(columnLabel);
	}

	@Override
	public Reader getCharacterStream(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getCharacterStream(columnIndex);
	}

	@Override
	public Reader getCharacterStream(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getCharacterStream(columnLabel);
	}

	@Override
	public Clob getClob(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getClob(columnIndex);
	}

	@Override
	public Clob getClob(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getClob(columnLabel);
	}

	@Override
	public int getConcurrency() throws SQLException {
		updateExpires();
		return rs.getConcurrency();
	}

	@Override
	public String getCursorName() throws SQLException {
		updateExpires();
		return rs.getCursorName();
	}

	@Override
	public Date getDate(int pos) throws SQLException {
		updateExpires();
		return rs.getDate(pos);
	}

	@Override
	public Date getDate(String name) throws SQLException {
		updateExpires();
		return rs.getDate(name);
	}

	@Override
	public Date getDate(int arg0, Calendar arg1) throws SQLException {
		updateExpires();
		return rs.getDate(arg0, arg1);
	}

	@Override
	public Date getDate(String arg0, Calendar arg1) throws SQLException {
		updateExpires();
		return rs.getDate(arg0, arg1);
	}

	@Override
	public double getDouble(int pos) throws SQLException {
		updateExpires();
		return rs.getDouble(pos);
	}

	@Override
	public double getDouble(String name) throws SQLException {
		updateExpires();
		return rs.getDouble(name);
	}

	@Override
	public int getFetchDirection() throws SQLException {
		updateExpires();
		return rs.getFetchDirection();
	}

	@Override
	public int getFetchSize() throws SQLException {
		updateExpires();
		return rs.getFetchSize();
	}

	@Override
	public float getFloat(int pos) throws SQLException {
		updateExpires();
		return rs.getFloat(pos);
	}

	@Override
	public float getFloat(String name) throws SQLException {
		updateExpires();
		return rs.getFloat(name);
	}

	@Override
	public int getHoldability() throws SQLException {
		updateExpires();
		return rs.getHoldability();
	}

	@Override
	public int getInt(int pos) throws SQLException {
		updateExpires();
		return rs.getInt(pos);
	}

	@Override
	public int getInt(String name) throws SQLException {
		updateExpires();
		return rs.getInt(name);
	}

	@Override
	public long getLong(int pos) throws SQLException {
		updateExpires();
		return rs.getLong(pos);
	}

	@Override
	public long getLong(String name) throws SQLException {
		updateExpires();
		return rs.getLong(name);
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		updateExpires();
		return ManagerResultSetMetaDatas.newResultSetMetaData(this);
	}

	@Override
	public Reader getNCharacterStream(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getNCharacterStream(columnIndex);
	}

	@Override
	public Reader getNCharacterStream(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getNCharacterStream(columnLabel);
	}

	@Override
	public NClob getNClob(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getNClob(columnIndex);
	}

	@Override
	public NClob getNClob(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getNClob(columnLabel);
	}

	@Override
	public String getNString(int pos) throws SQLException {
		updateExpires();
		return rs.getNString(pos);
	}

	@Override
	public String getNString(String name) throws SQLException {
		updateExpires();
		return rs.getNString(name);
	}

	@Override
	public Object getObject(int pos) throws SQLException {
		updateExpires();
		return rs.getObject(pos);
	}

	@Override
	public Object getObject(String name) throws SQLException {
		updateExpires();
		return rs.getObject(name);
	}

	@Override
	public Object getObject(int columnIndex, Map<String, Class<?>> map) throws SQLException {
		updateExpires();
		return rs.getObject(columnIndex, map);
	}

	@Override
	public Object getObject(String columnLabel, Map<String, Class<?>> map) throws SQLException {
		updateExpires();
		return rs.getObject(columnLabel, map);
	}

	@Override
	public <T> T getObject(int columnIndex, Class<T> type) throws SQLException {
		updateExpires();
		return rs.getObject(columnIndex, type);
	}

	@Override
	public <T> T getObject(String columnLabel, Class<T> type) throws SQLException {
		updateExpires();
		return rs.getObject(columnLabel, type);
	}

	@Override
	public Ref getRef(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getRef(columnIndex);
	}

	@Override
	public Ref getRef(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getRef(columnLabel);
	}

	@Override
	public int getRow() throws SQLException {
		updateExpires();
		return rs.getRow();
	}

	@Override
	public RowId getRowId(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getRowId(columnIndex);
	}

	@Override
	public RowId getRowId(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getRowId(columnLabel);
	}

	@Override
	public SQLXML getSQLXML(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getSQLXML(columnIndex);
	}

	@Override
	public SQLXML getSQLXML(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getSQLXML(columnLabel);
	}

	@Override
	public short getShort(int pos) throws SQLException {
		updateExpires();
		return rs.getShort(pos);
	}

	@Override
	public short getShort(String name) throws SQLException {
		updateExpires();
		return rs.getShort(name);
	}

	@Override
	public Statement getStatement() throws SQLException {
		updateExpires();
		return stmt;
	}

	@Override
	public String getString(int pos) throws SQLException {
		updateExpires();
		return rs.getString(pos);
	}

	@Override
	public String getString(String name) throws SQLException {
		updateExpires();
		return rs.getString(name);
	}

	@Override
	public Time getTime(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getTime(columnIndex);
	}

	@Override
	public Time getTime(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getTime(columnLabel);
	}

	@Override
	public Time getTime(int columnIndex, Calendar cal) throws SQLException {
		updateExpires();
		return rs.getTime(columnIndex, cal);
	}

	@Override
	public Time getTime(String columnLabel, Calendar cal) throws SQLException {
		updateExpires();
		return rs.getTime(columnLabel, cal);
	}

	@Override
	public Timestamp getTimestamp(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getTimestamp(columnIndex);
	}

	@Override
	public Timestamp getTimestamp(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getTimestamp(columnLabel);
	}

	@Override
	public Timestamp getTimestamp(int columnIndex, Calendar cal) throws SQLException {
		updateExpires();
		return rs.getTimestamp(columnIndex, cal);
	}

	@Override
	public Timestamp getTimestamp(String columnLabel, Calendar cal) throws SQLException {
		updateExpires();
		return rs.getTimestamp(columnLabel, cal);
	}

	@Override
	public int getType() throws SQLException {
		updateExpires();
		return rs.getType();
	}

	@Override
	public URL getURL(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getURL(columnIndex);
	}

	@Override
	public URL getURL(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getURL(columnLabel);
	}

	@Override
	public InputStream getUnicodeStream(int columnIndex) throws SQLException {
		updateExpires();
		return rs.getUnicodeStream(columnIndex);
	}

	@Override
	public InputStream getUnicodeStream(String columnLabel) throws SQLException {
		updateExpires();
		return rs.getUnicodeStream(columnLabel);
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		updateExpires();
		return rs.getWarnings();
	}

	@Override
	public void insertRow() throws SQLException {
		updateExpires();
		rs.insertRow();
	}

	@Override
	public boolean isAfterLast() throws SQLException {
		updateExpires();
		return rs.isAfterLast();
	}

	@Override
	public boolean isBeforeFirst() throws SQLException {
		updateExpires();
		return rs.isBeforeFirst();
	}

	@Override
	public boolean isClosed() throws SQLException {
		updateExpires();
		return rs.isClosed();
	}

	@Override
	public boolean isFirst() throws SQLException {
		updateExpires();
		return rs.isFirst();
	}

	@Override
	public boolean isLast() throws SQLException {
		updateExpires();
		return rs.isLast();
	}

	@Override
	public boolean last() throws SQLException {
		updateExpires();
		return rs.last();
	}

	@Override
	public void moveToCurrentRow() throws SQLException {
		updateExpires();
		rs.moveToCurrentRow();
		
	}

	@Override
	public void moveToInsertRow() throws SQLException {
		updateExpires();
		rs.moveToInsertRow();
	}

	@Override
	public boolean next() throws SQLException {
		updateExpires();
		return rs.next();
	}

	@Override
	public boolean previous() throws SQLException {
		updateExpires();
		return rs.previous();
	}

	@Override
	public void refreshRow() throws SQLException {
		updateExpires();
		rs.refreshRow();		
	}

	@Override
	public boolean relative(int rows) throws SQLException {
		updateExpires();
		return rs.relative(rows);
	}

	@Override
	public boolean rowDeleted() throws SQLException {
		updateExpires();
		return rs.rowDeleted();
	}

	@Override
	public boolean rowInserted() throws SQLException {
		updateExpires();
		return rs.rowInserted();
	}

	@Override
	public boolean rowUpdated() throws SQLException {
		updateExpires();
		return rs.rowUpdated();
	}

	@Override
	public void setFetchDirection(int direction) throws SQLException {
		updateExpires();
		rs.setFetchDirection(direction);	
	}

	@Override
	public void setFetchSize(int direction) throws SQLException {
		updateExpires();
		rs.setFetchDirection(direction);		
	}

	@Override
	public void updateArray(int columnIndex, Array x) throws SQLException {
		updateExpires();
		rs.updateArray(columnIndex, x);		
	}

	@Override
	public void updateArray(String columnLabel, Array x) throws SQLException {
		updateExpires();
		rs.updateArray(columnLabel, x);		
	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x) throws SQLException {
		updateExpires();
		rs.updateAsciiStream(columnIndex, x);	
	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x) throws SQLException {
		updateExpires();
		rs.updateAsciiStream(columnLabel, x);		
	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, int length) throws SQLException {
		updateExpires();
		rs.updateAsciiStream(columnIndex, x, length);		
	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, int length) throws SQLException {
		updateExpires();
		rs.updateAsciiStream(columnLabel, x, length);		
	}

	@Override
	public void updateAsciiStream(int columnIndex, InputStream x, long length) throws SQLException {
		updateExpires();
		rs.updateAsciiStream(columnIndex, x, length);		
	}

	@Override
	public void updateAsciiStream(String columnLabel, InputStream x, long length) throws SQLException {
		updateExpires();
		rs.updateAsciiStream(columnLabel, x, length);		
	}

	@Override
	public void updateBigDecimal(int columnIndex, BigDecimal x) throws SQLException {
		updateExpires();
		rs.updateBigDecimal(columnIndex, x);		
	}

	@Override
	public void updateBigDecimal(String columnLabel, BigDecimal x) throws SQLException {
		updateExpires();
		rs.updateBigDecimal(columnLabel, x);		
	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x) throws SQLException {
		updateExpires();
		rs.updateBinaryStream(columnIndex, x);		
	}

	@Override
	public void updateBinaryStream(String columnLabel, InputStream x) throws SQLException {
		updateExpires();
		rs.updateBinaryStream(columnLabel, x);		
	}

	@Override
	public void updateBinaryStream(int columnIndex, InputStream x, int length) throws SQLException {
		updateExpires();
		rs.updateBinaryStream(columnIndex, x, length);
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		updateExpires();
		rs.updateBinaryStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {		
		updateExpires();
		rs.updateBinaryStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateBinaryStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateBinaryStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateBlob(int arg0, Blob arg1) throws SQLException {
		updateExpires();
		rs.updateBlob(arg0, arg1);		
	}

	@Override
	public void updateBlob(String arg0, Blob arg1) throws SQLException {
		updateExpires();
		rs.updateBlob(arg0, arg1);		
	}

	@Override
	public void updateBlob(int arg0, InputStream arg1) throws SQLException {
		updateExpires();
		rs.updateBlob(arg0, arg1);		
	}

	@Override
	public void updateBlob(String arg0, InputStream arg1) throws SQLException {
		updateExpires();
		rs.updateBlob(arg0, arg1);		
	}

	@Override
	public void updateBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateBlob(arg0, arg1, arg2);		
	}

	@Override
	public void updateBlob(String arg0, InputStream arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateBlob(arg0, arg1, arg2);		
	}

	@Override
	public void updateBoolean(int arg0, boolean arg1) throws SQLException {
		updateExpires();
		rs.updateBoolean(arg0, arg1);		
	}

	@Override
	public void updateBoolean(String arg0, boolean arg1) throws SQLException {
		updateExpires();
		rs.updateBoolean(arg0, arg1);		
	}

	@Override
	public void updateByte(int arg0, byte arg1) throws SQLException {
		updateExpires();
		rs.updateByte(arg0, arg1);		
	}

	@Override
	public void updateByte(String arg0, byte arg1) throws SQLException {
		updateExpires();
		rs.updateByte(arg0, arg1);		
	}

	@Override
	public void updateBytes(int arg0, byte[] arg1) throws SQLException {
		updateExpires();
		rs.updateBytes(arg0, arg1);		
	}

	@Override
	public void updateBytes(String arg0, byte[] arg1) throws SQLException {
		updateExpires();
		rs.updateBytes(arg0, arg1);		
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateCharacterStream(arg0, arg1);		
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateCharacterStream(arg0, arg1);		
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		updateExpires();
		rs.updateCharacterStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1, int arg2) throws SQLException {
		updateExpires();
		rs.updateCharacterStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateCharacterStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateCharacterStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateClob(int arg0, Clob arg1) throws SQLException {
		updateExpires();
		rs.updateClob(arg0, arg1);		
	}

	@Override
	public void updateClob(String arg0, Clob arg1) throws SQLException {
		updateExpires();
		rs.updateClob(arg0, arg1);		
	}

	@Override
	public void updateClob(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateClob(arg0, arg1);		
	}

	@Override
	public void updateClob(String arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateClob(arg0, arg1);		
	}

	@Override
	public void updateClob(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateClob(arg0, arg1, arg2);		
	}

	@Override
	public void updateClob(String arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateClob(arg0, arg1, arg2);		
	}

	@Override
	public void updateDate(int arg0, Date arg1) throws SQLException {
		updateExpires();
		rs.updateDate(arg0, arg1);		
	}

	@Override
	public void updateDate(String arg0, Date arg1) throws SQLException {
		updateExpires();
		rs.updateDate(arg0, arg1);		
	}

	@Override
	public void updateDouble(int arg0, double arg1) throws SQLException {
		updateExpires();
		rs.updateDouble(arg0, arg1);		
	}

	@Override
	public void updateDouble(String arg0, double arg1) throws SQLException {
		updateExpires();
		rs.updateDouble(arg0, arg1);		
	}

	@Override
	public void updateFloat(int arg0, float arg1) throws SQLException {
		updateExpires();
		rs.updateFloat(arg0, arg1);		
	}

	@Override
	public void updateFloat(String arg0, float arg1) throws SQLException {
		updateExpires();
		rs.updateFloat(arg0, arg1);		
	}

	@Override
	public void updateInt(int arg0, int arg1) throws SQLException {
		updateExpires();
		rs.updateInt(arg0, arg1);		
	}

	@Override
	public void updateInt(String arg0, int arg1) throws SQLException {
		updateExpires();
		rs.updateInt(arg0, arg1);		
	}

	@Override
	public void updateLong(int arg0, long arg1) throws SQLException {
		updateExpires();
		rs.updateLong(arg0, arg1);		
	}

	@Override
	public void updateLong(String arg0, long arg1) throws SQLException {
		updateExpires();
		rs.updateLong(arg0, arg1);		
	}

	@Override
	public void updateNCharacterStream(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateNCharacterStream(arg0, arg1);		
	}

	@Override
	public void updateNCharacterStream(String arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateNCharacterStream(arg0, arg1);		
	}

	@Override
	public void updateNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateNCharacterStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateNCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateNCharacterStream(arg0, arg1, arg2);		
	}

	@Override
	public void updateNClob(int arg0, NClob arg1) throws SQLException {
		updateExpires();
		rs.updateNClob(arg0, arg1);		
	}

	@Override
	public void updateNClob(String arg0, NClob arg1) throws SQLException {
		updateExpires();
		rs.updateNClob(arg0, arg1);		
	}

	@Override
	public void updateNClob(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateNClob(arg0, arg1);		
	}

	@Override
	public void updateNClob(String arg0, Reader arg1) throws SQLException {
		updateExpires();
		rs.updateNClob(arg0, arg1);		
	}

	@Override
	public void updateNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateNClob(arg0, arg1, arg2);		
	}

	@Override
	public void updateNClob(String arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		rs.updateNClob(arg0, arg1, arg2);		
	}

	@Override
	public void updateNString(int arg0, String arg1) throws SQLException {
		updateExpires();
		rs.updateNString(arg0, arg1);		
	}

	@Override
	public void updateNString(String arg0, String arg1) throws SQLException {
		updateExpires();
		rs.updateNString(arg0, arg1);		
	}

	@Override
	public void updateNull(int arg0) throws SQLException {
		updateExpires();
		rs.updateNull(arg0);		
	}

	@Override
	public void updateNull(String arg0) throws SQLException {
		updateExpires();
		rs.updateNull(arg0);		
	}

	@Override
	public void updateObject(int arg0, Object arg1) throws SQLException {
		updateExpires();
		rs.updateObject(arg0, arg1);		
	}

	@Override
	public void updateObject(String arg0, Object arg1) throws SQLException {
		updateExpires();
		rs.updateObject(arg0, arg1);		
	}

	@Override
	public void updateObject(int arg0, Object arg1, int arg2) throws SQLException {
		updateExpires();
		rs.updateObject(arg0, arg1, arg2);		
	}

	@Override
	public void updateObject(String arg0, Object arg1, int arg2) throws SQLException {
		updateExpires();
		rs.updateObject(arg0, arg1, arg2);		
	}

	@Override
	public void updateRef(int arg0, Ref arg1) throws SQLException {
		updateExpires();
		rs.updateRef(arg0, arg1);		
	}

	@Override
	public void updateRef(String arg0, Ref arg1) throws SQLException {
		updateExpires();
		rs.updateRef(arg0, arg1);		
	}

	@Override
	public void updateRow() throws SQLException {
		updateExpires();
		rs.updateRow();		
	}

	@Override
	public void updateRowId(int arg0, RowId arg1) throws SQLException {
		updateExpires();
		rs.updateRowId(arg0, arg1);		
	}

	@Override
	public void updateRowId(String arg0, RowId arg1) throws SQLException {
		updateExpires();
		rs.updateRowId(arg0, arg1);		
	}

	@Override
	public void updateSQLXML(int arg0, SQLXML arg1) throws SQLException {
		updateExpires();
		rs.updateSQLXML(arg0, arg1);		
	}

	@Override
	public void updateSQLXML(String arg0, SQLXML arg1) throws SQLException {
		updateExpires();
		rs.updateSQLXML(arg0, arg1);		
	}

	@Override
	public void updateShort(int arg0, short arg1) throws SQLException {
		updateExpires();
		rs.updateShort(arg0, arg1);		
	}

	@Override
	public void updateShort(String arg0, short arg1) throws SQLException {
		updateExpires();
		rs.updateShort(arg0, arg1);		
	}

	@Override
	public void updateString(int arg0, String arg1) throws SQLException {
		updateExpires();
		rs.updateString(arg0, arg1);		
	}

	@Override
	public void updateString(String arg0, String arg1) throws SQLException {
		updateExpires();
		rs.updateString(arg0, arg1);		
	}

	@Override
	public void updateTime(int arg0, Time arg1) throws SQLException {
		updateExpires();
		rs.updateTime(arg0, arg1);		
	}

	@Override
	public void updateTime(String arg0, Time arg1) throws SQLException {
		updateExpires();
		rs.updateTime(arg0, arg1);		
	}

	@Override
	public void updateTimestamp(int arg0, Timestamp arg1) throws SQLException {
		updateExpires();
		rs.updateTimestamp(arg0, arg1);		
	}

	@Override
	public void updateTimestamp(String arg0, Timestamp arg1) throws SQLException {
		updateExpires();
		rs.updateTimestamp(arg0, arg1);		
	}

	@Override
	public boolean wasNull() throws SQLException {
		updateExpires();
		return rs.wasNull();		
	}

}
