package se.prv.jdbcsimulering.serverobjects;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSClob implements Clob {

	private static Logger logg = Logger.getLogger(JDBCSBlob.class); 

	private Clob clob = null;
	private String reference = null;
	private long validity = 0l;
	protected long expires = 0l;

	public JDBCSClob(Clob clob, long validity) {
		this.validity = validity;
		this.clob = clob;
		this.reference = GenerateReference.generate();
		expires = System.currentTimeMillis()+validity;
	}

	public String getReference() {
		return reference;
	}
	
	public long getValidity() {
		return validity;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}

	@Override
	public void free() throws SQLException {
		updateExpires();
	}

	@Override
	public InputStream getAsciiStream() throws SQLException {
		updateExpires();
		return clob.getAsciiStream();
	}

	@Override
	public Reader getCharacterStream() throws SQLException {
		updateExpires();
		return clob.getCharacterStream();
	}

	@Override
	public Reader getCharacterStream(long pos, long length) throws SQLException {
		updateExpires();
		return clob.getCharacterStream(pos, length);
	}

	@Override
	public String getSubString(long pos, int length) throws SQLException {
		updateExpires();
		return clob.getSubString(pos, length);
	}

	@Override
	public long length() throws SQLException {
		updateExpires();
		return clob.length();
	}

	@Override
	public long position(String searchstr, long start) throws SQLException {
		updateExpires();
		return clob.position(searchstr, start);
	}

	@Override
	public long position(Clob searchstr, long start) throws SQLException {
		updateExpires();
		return clob.position(searchstr, start);
	}

	@Override
	public OutputStream setAsciiStream(long pos) throws SQLException {
		updateExpires();
		return clob.setAsciiStream(pos);
	}

	@Override
	public Writer setCharacterStream(long pos) throws SQLException {
		updateExpires();
		return clob.setCharacterStream(pos);
	}

	@Override
	public int setString(long pos, String str) throws SQLException {
		updateExpires();
		return clob.setString(pos, str);
	}

	@Override
	public int setString(long pos, String str, int offset, int len) throws SQLException {
		updateExpires();
		return clob.setString(pos, str, offset, len);
	}

	@Override
	public void truncate(long len) throws SQLException {
		updateExpires();
		clob.truncate(len);
	}

}
