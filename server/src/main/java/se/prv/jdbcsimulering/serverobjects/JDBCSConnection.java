package se.prv.jdbcsimulering.serverobjects;

import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.pool.ManagerStatements;
import se.prv.jdbcsimulering.utils.GenerateReference;

public class JDBCSConnection implements Connection {

	private static Logger logg = Logger.getLogger(JDBCSConnection.class); 
	
	private String reference = null;
	private Connection conn = null;
	private long validity = 0l;
	private long expires = 0l;

	public JDBCSConnection(String reference, Connection conn, long validity) {
		this.reference = reference;
		this.conn = conn;
		this.validity = validity;
		updateExpires();
	}
	
	public String getReference() {
		return reference;
	}
	
	public long getValidity() {
		return validity;
	}
	
	public boolean isExpired() {
		return System.currentTimeMillis() > expires;
	}
	
	protected void updateExpires() {
		expires = System.currentTimeMillis()+validity;
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void abort(Executor arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void clearWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void close() throws SQLException {
		updateExpires();
		logg.info("Operation close, ref "+reference);
		conn.close();
	}

	@Override
	public void commit() throws SQLException {
		updateExpires();
		logg.info("Operation commit, ref "+reference);
		conn.commit();
	}

	@Override
	public Array createArrayOf(String arg0, Object[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Blob createBlob() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Clob createClob() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public NClob createNClob() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Statement createStatement() throws SQLException {
		updateExpires();
		return ManagerStatements.newStatement_createStatement(this);
	}

	@Override
	public Statement createStatement(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Statement createStatement(int arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Struct createStruct(String arg0, Object[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getCatalog() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getClientInfo(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getSchema() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isValid(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String nativeSQL(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public CallableStatement prepareCall(String sql) throws SQLException {
		updateExpires();
		logg.info("sql="+sql);
		return ManagerStatements.newCallableStatement_prepareCall_sql(this, sql);
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public PreparedStatement prepareStatement(String sql) throws SQLException {
		updateExpires();
		logg.info("sql="+sql);
		return ManagerStatements.newPreparedStatement_prepareStatement_sql(this, sql);
	}

	@Override
	public PreparedStatement prepareStatement(String sql, int autoGeneratedKeys) throws SQLException {
		updateExpires();
		logg.info("sql="+sql+", autoGeneratedKeys="+autoGeneratedKeys);
		return ManagerStatements.newPreparedStatement_prepareStatement_sql_autoGeneratedKeys(this, sql,autoGeneratedKeys);
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, String[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void releaseSavepoint(Savepoint arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void rollback() throws SQLException {
		updateExpires();
		logg.info("Operation rollback, ref "+reference);
		conn.rollback();
	}

	@Override
	public void rollback(Savepoint arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setAutoCommit(boolean flag) throws SQLException {
		updateExpires();
		logg.info("flag="+flag);
	}

	@Override
	public void setCatalog(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setClientInfo(Properties arg0) throws SQLClientInfoException {
		throw new SQLClientInfoException("Saknar implementering på serversidan", null);		
	}

	@Override
	public void setClientInfo(String arg0, String arg1) throws SQLClientInfoException {
		throw new SQLClientInfoException("Saknar implementering på serversidan", null);		
	}

	@Override
	public void setHoldability(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setNetworkTimeout(Executor arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setReadOnly(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Savepoint setSavepoint(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setSchema(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setTransactionIsolation(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");		
	}

	public Connection getConnection() {
		updateExpires();
		return conn;
	}

}
