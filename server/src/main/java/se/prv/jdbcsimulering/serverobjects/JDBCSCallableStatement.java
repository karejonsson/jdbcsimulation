package se.prv.jdbcsimulering.serverobjects;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Map;

import org.apache.log4j.Logger;

import se.prv.jdbcsimulering.pool.ManagerResultSetMetaDatas;
import se.prv.jdbcsimulering.pool.ManagerResultSets;

public class JDBCSCallableStatement extends JDBCSPreparedStatement implements CallableStatement {

	private static Logger logg = Logger.getLogger(JDBCSCallableStatement.class); 

	public JDBCSCallableStatement(JDBCSConnection conn, CallableStatement cstmt, long validity) {
		super(conn, cstmt, validity);
	}

	@Override
	public void addBatch() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void clearParameters() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean execute() throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).execute();
	}

	@Override
	public ResultSet executeQuery() throws SQLException {
		logg.info("");
		return ManagerResultSets.newResultSet_executeQuery(this);
	}

	@Override
	public int executeUpdate() throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeUpdate();
	}

	@Override
	public ResultSetMetaData getMetaData() throws SQLException {
		updateExpires();
		return ManagerResultSetMetaDatas.newResultSetMetaData(this);
	}

	@Override
	public ParameterMetaData getParameterMetaData() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setArray(int arg0, Array arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setAsciiStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBigDecimal(int arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBinaryStream(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBlob(int arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBlob(int arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBlob(int arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBoolean(int arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setByte(int pos, byte val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setByte(pos, val);
	}

	@Override
	public void setBytes(int pos, byte[] val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setBytes(pos, val);
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setCharacterStream(arg0, arg1);
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, int arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setCharacterStream(arg0, arg1, arg2);
	}

	@Override
	public void setCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setCharacterStream(arg0, arg1, arg2);
	}

	@Override
	public void setClob(int arg0, Clob arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setClob(arg0, arg1);
	}

	@Override
	public void setClob(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setClob(arg0, arg1);
	}

	@Override
	public void setClob(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setClob(arg0, arg1, arg2);
	}

	@Override
	public void setDate(int pos, Date val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setDate(pos, val);
	}

	@Override
	public void setDate(int arg0, Date arg1, Calendar arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setDate(arg0, arg1, arg2);
	}

	@Override
	public void setDouble(int pos, double val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setDouble(pos, val);
	}

	@Override
	public void setFloat(int pos, float val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setFloat(pos, val);
	}

	@Override
	public void setInt(int pos, int val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setInt(pos, val);
	}

	@Override
	public void setLong(int pos, long val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setLong(pos, val);
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNCharacterStream(arg0, arg1);
	}

	@Override
	public void setNCharacterStream(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNCharacterStream(arg0, arg1, arg2);
	}

	@Override
	public void setNClob(int arg0, NClob arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNClob(arg0, arg1);
	}

	@Override
	public void setNClob(int arg0, Reader arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNClob(arg0, arg1);
	}

	@Override
	public void setNClob(int arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNClob(arg0, arg1, arg2);
	}

	@Override
	public void setNString(int pos, String val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNString(pos, val);
	}

	@Override
	public void setNull(int pos, int val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNull(pos, val);
	}

	@Override
	public void setNull(int arg0, int arg1, String arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNull(arg0, arg1, arg2);
	}

	@Override
	public void setObject(int pos, Object obj) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setObject(pos, obj);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setObject(arg0, arg1, arg2);
	}

	@Override
	public void setObject(int arg0, Object arg1, int arg2, int arg3) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setObject(arg0, arg1, arg2, arg3);
	}

	@Override
	public void setRef(int arg0, Ref arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setRef(arg0, arg1);
	}

	@Override
	public void setRowId(int arg0, RowId arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setRowId(arg0, arg1);
	}

	@Override
	public void setSQLXML(int arg0, SQLXML arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setSQLXML(arg0, arg1);
	}

	@Override
	public void setShort(int pos, short val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setShort(pos, val);
	}

	@Override
	public void setString(int pos, String val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setString(pos, val);
	}

	@Override
	public void setTime(int arg0, Time arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTime(arg0, arg1);
	}

	@Override
	public void setTime(int arg0, Time arg1, Calendar arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTime(arg0, arg1, arg2);
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTimestamp(arg0, arg1);
	}

	@Override
	public void setTimestamp(int arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTimestamp(arg0, arg1, arg2);
	}

	@Override
	public void setURL(int arg0, URL arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setURL(arg0, arg1);
	}

	@Override
	public void setUnicodeStream(int arg0, InputStream arg1, int arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setUnicodeStream(arg0, arg1, arg2);
	}

	@Override
	public void addBatch(String arg0) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).addBatch(arg0);
	}

	@Override
	public void cancel() throws SQLException {
		updateExpires();
		((CallableStatement) stmt).cancel();
	}

	@Override
	public void clearBatch() throws SQLException {
		updateExpires();
		((CallableStatement) stmt).clearBatch();
	}

	@Override
	public void clearWarnings() throws SQLException {
		updateExpires();
		((CallableStatement) stmt).clearWarnings();
	}

	@Override
	public void close() throws SQLException {
		updateExpires();
		((CallableStatement) stmt).close();
	}

	@Override
	public void closeOnCompletion() throws SQLException {
		updateExpires();
		((CallableStatement) stmt).closeOnCompletion();
	}

	@Override
	public boolean execute(String arg0) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).execute(arg0);
	}

	@Override
	public boolean execute(String arg0, int arg1) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).execute(arg0, arg1);
	}

	@Override
	public boolean execute(String arg0, int[] arg1) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).execute(arg0, arg1);
	}

	@Override
	public boolean execute(String arg0, String[] arg1) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).execute(arg0, arg1);
	}

	@Override
	public int[] executeBatch() throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeBatch();
	}

	@Override
	public ResultSet executeQuery(String arg0) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeQuery(arg0);
	}

	@Override
	public int executeUpdate(String arg0) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeUpdate(arg0);
	}

	@Override
	public int executeUpdate(String arg0, int arg1) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeUpdate(arg0, arg1);
	}

	@Override
	public int executeUpdate(String arg0, int[] arg1) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeUpdate(arg0, arg1);
	}

	@Override
	public int executeUpdate(String arg0, String[] arg1) throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).executeUpdate(arg0, arg1);
	}

	@Override
	public Connection getConnection() throws SQLException {
		updateExpires();
		return super.getConnection();
	}

	@Override
	public int getFetchDirection() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getFetchSize() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public ResultSet getGeneratedKeys() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getMaxFieldSize() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getMaxRows() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean getMoreResults() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean getMoreResults(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getQueryTimeout() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public ResultSet getResultSet() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getResultSetConcurrency() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getResultSetHoldability() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getResultSetType() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getUpdateCount() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isCloseOnCompletion() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isClosed() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isPoolable() throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setCursorName(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setEscapeProcessing(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setFetchDirection(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setFetchSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setMaxFieldSize(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setMaxRows(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setPoolable(boolean arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setQueryTimeout(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Array getArray(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Array getArray(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public BigDecimal getBigDecimal(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public BigDecimal getBigDecimal(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public BigDecimal getBigDecimal(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Blob getBlob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Blob getBlob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean getBoolean(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public boolean getBoolean(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public byte getByte(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public byte getByte(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public byte[] getBytes(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public byte[] getBytes(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Reader getCharacterStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Reader getCharacterStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Clob getClob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Clob getClob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Date getDate(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Date getDate(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Date getDate(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Date getDate(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public double getDouble(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public double getDouble(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public float getFloat(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public float getFloat(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getInt(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public int getInt(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public long getLong(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public long getLong(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Reader getNCharacterStream(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Reader getNCharacterStream(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public NClob getNClob(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public NClob getNClob(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getNString(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getNString(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Object getObject(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Object getObject(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Object getObject(int arg0, Map<String, Class<?>> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Object getObject(String arg0, Map<String, Class<?>> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public <T> T getObject(int arg0, Class<T> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public <T> T getObject(String arg0, Class<T> arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Ref getRef(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Ref getRef(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public RowId getRowId(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public RowId getRowId(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public SQLXML getSQLXML(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public SQLXML getSQLXML(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public short getShort(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public short getShort(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getString(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public String getString(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Time getTime(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Time getTime(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Time getTime(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Time getTime(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Timestamp getTimestamp(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Timestamp getTimestamp(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Timestamp getTimestamp(int arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public Timestamp getTimestamp(String arg0, Calendar arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public URL getURL(int arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public URL getURL(String arg0) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void registerOutParameter(int arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void registerOutParameter(String arg0, int arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void registerOutParameter(int arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void registerOutParameter(int arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void registerOutParameter(String arg0, int arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void registerOutParameter(String arg0, int arg1, String arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setAsciiStream(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setAsciiStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setAsciiStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBigDecimal(String arg0, BigDecimal arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBinaryStream(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBinaryStream(String arg0, InputStream arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBinaryStream(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBlob(String arg0, Blob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBlob(String arg0, InputStream arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBlob(String arg0, InputStream arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBoolean(String arg0, boolean arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setByte(String arg0, byte arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setBytes(String arg0, byte[] arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setCharacterStream(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setCharacterStream(String arg0, Reader arg1, int arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setClob(String arg0, Clob arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setClob(String arg0, Reader arg1) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setClob(String arg0, Reader arg1, long arg2) throws SQLException {
		throw new SQLException("Saknar implementering på serversidan");
	}

	@Override
	public void setDate(String name, Date val) throws SQLException {
		((CallableStatement) stmt).setDate(name, val);
	}

	@Override
	public void setDate(String arg0, Date arg1, Calendar arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setDate(arg0, arg1, arg2);
	}

	@Override
	public void setDouble(String name, double val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setDouble(name, val);
	}

	@Override
	public void setFloat(String name, float val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setFloat(name, val);
	}

	@Override
	public void setInt(String name, int val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setInt(name, val);
	}

	@Override
	public void setLong(String name, long val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setLong(name, val);
	}

	@Override
	public void setNCharacterStream(String arg0, Reader arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNCharacterStream(arg0, arg1);
	}

	@Override
	public void setNCharacterStream(String arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNCharacterStream(arg0, arg1, arg2);
	}

	@Override
	public void setNClob(String arg0, NClob arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNClob(arg0, arg1);
	}

	@Override
	public void setNClob(String arg0, Reader arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNClob(arg0, arg1);
	}

	@Override
	public void setNClob(String arg0, Reader arg1, long arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNClob(arg0, arg1, arg2);
	}

	@Override
	public void setNString(String name, String val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNString(name, val);
	}

	@Override
	public void setNull(String name, int val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNull(name, val);
	}

	@Override
	public void setNull(String arg0, int arg1, String arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setNull(arg0, arg1, arg2);
	}

	@Override
	public void setObject(String name, Object val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setObject(name, val);
	}

	@Override
	public void setObject(String arg0, Object arg1, int arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setObject(arg0, arg1, arg2);
	}

	@Override
	public void setObject(String arg0, Object arg1, int arg2, int arg3) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setObject(arg0, arg1, arg2, arg3);
	}

	@Override
	public void setRowId(String arg0, RowId arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setRowId(arg0, arg1);
	}

	@Override
	public void setSQLXML(String arg0, SQLXML arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setSQLXML(arg0, arg1);
	}

	@Override
	public void setShort(String name, short val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setShort(name, val);
	}

	@Override
	public void setString(String name, String val) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setString(name, val);
	}

	@Override
	public void setTime(String arg0, Time arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTime(arg0, arg1);
	}

	@Override
	public void setTime(String arg0, Time arg1, Calendar arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTime(arg0, arg1, arg2);
	}

	@Override
	public void setTimestamp(String arg0, Timestamp arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTimestamp(arg0, arg1);
	}

	@Override
	public void setTimestamp(String arg0, Timestamp arg1, Calendar arg2) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setTimestamp(arg0, arg1, arg2);
	}

	@Override
	public void setURL(String arg0, URL arg1) throws SQLException {
		updateExpires();
		((CallableStatement) stmt).setURL(arg0, arg1);
	}

	@Override
	public boolean wasNull() throws SQLException {
		updateExpires();
		return ((CallableStatement) stmt).wasNull();
	}

}
