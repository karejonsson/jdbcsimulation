package se.prv.jdbcsimulering.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.prv.jdbcsimulering.pool.ManagerConnections;
import se.prv.jdbcsimulering.serverobjects.JDBCSCallableStatement;
import se.prv.jdbcsimulering.serverobjects.JDBCSConnection;
import se.prv.jdbcsimulering.serverobjects.JDBCSPreparedStatement;
import se.prv.jdbcsimulering.serverobjects.JDBCSStatement;
import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;
import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;

@RestController
@RequestMapping(value = "/Connection")
public class Connection {

	private static Logger logg = Logger.getLogger(WebserviceManager.class); 

	private static String jwtsecret = null;
	
	static {
		jwtsecret = JwtTokenUtil.jwtsecret;
	}

	@RequestMapping(value = "/close", method={ RequestMethod.POST })
	public ResponseEntity<Exception> close(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			conn.close();
		}
		catch(Exception e) {
			logg.error(e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/createStatement", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> createStatement(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		JDBCSStatement stmt = null;
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			stmt = (JDBCSStatement) conn.createStatement();
		}
		catch(Exception e) {
			logg.error("Inget statement från uppletad Connection", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    if(stmt == null) {
    		logg.warn("Fick inget statement");
	    	return ResponseEntity.noContent().build();
	    }
	    try {
		    Map<String, Object> out = new HashMap<>();
		    try {
		    	String ref = stmt.getReference();
		    	logg.info("Ref="+ref);
			    out.put(Constants.reference, SymetricEncryptionUtil.encrypt(ref, he.secret));
		    }
		    catch(Exception e) {
	    		logg.error("FEL", e);
	    		out.put(Constants.exception, e);
		    	return new ResponseEntity<>(out, HttpStatus.BAD_REQUEST);
		    }
	    	logg.info("ok");
			return new ResponseEntity<>(out, HttpStatus.OK);
	    }
	    catch(Exception e) {
	    	logg.error(e);
	    }
	    return null;
	}
	
	private static ResponseEntity<Map<String, Object>> finishReference(JDBCSPreparedStatement stmt, String secret) {
	    if(stmt == null) {
    		logg.warn("Fick inget statement");
	    	return ResponseEntity.noContent().build();
	    }
	    try {
		    Map<String, Object> out = new HashMap<>();
		    try {
		    	String ref = stmt.getReference();
		    	logg.info("Ref="+ref);
			    out.put(Constants.reference, SymetricEncryptionUtil.encrypt(ref, secret));
		    }
		    catch(Exception e) {
	    		logg.error("FEL", e);
	    		out.put(Constants.exception, e);
		    	return new ResponseEntity<>(out, HttpStatus.BAD_REQUEST);
		    }
	    	logg.info("ok");
			return new ResponseEntity<>(out, HttpStatus.OK);
	    }
	    catch(Exception e) {
	    	logg.error(e);
	    }
	    return null;
	}

	@RequestMapping(value = "/prepareStatement", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> prepareStatement(
			@RequestHeader Map<String, String> headers, 
			@RequestParam("sql") String encryptedsql) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he+", encryptedsql="+encryptedsql);
		JDBCSPreparedStatement stmt = null;
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			String sql = SymetricEncryptionUtil.decrypt(encryptedsql, he.secret);
			logg.info("sql="+sql);
			stmt = (JDBCSPreparedStatement) conn.prepareStatement(sql);
		}
		catch(Exception e) {
			logg.error("Inget statement från uppletad Connection", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
		return finishReference(stmt, he.secret);
	}

	@RequestMapping(value = "/prepareStatementWithKeys", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> prepareStatement(
			@RequestHeader Map<String, String> headers, 
			@RequestParam("sql") String encryptedsql,
			@RequestParam("autoGeneratedKeys") int autoGeneratedKeys) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he+", encryptedsql="+encryptedsql);
		JDBCSPreparedStatement stmt = null;
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			String sql = SymetricEncryptionUtil.decrypt(encryptedsql, he.secret);
			logg.info("sql="+sql);
			stmt = (JDBCSPreparedStatement) conn.prepareStatement(sql, autoGeneratedKeys);
		}
		catch(Exception e) {
			logg.error("Inget statement från uppletad Connection", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
		return finishReference(stmt, he.secret);
	}
	
	@RequestMapping(value = "/prepareCall", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> prepareCall(
			@RequestHeader Map<String, String> headers, 
			@RequestParam("sql") String encryptedsql) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he+", encryptedsql="+encryptedsql);
		JDBCSCallableStatement stmt = null;
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			String sql = SymetricEncryptionUtil.decrypt(encryptedsql, he.secret);
			logg.info("sql="+sql);
			stmt = (JDBCSCallableStatement) conn.prepareCall(sql);
		}
		catch(Exception e) {
			logg.error("Inget statement från uppletad Connection", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
		return finishReference(stmt, he.secret);
	}

	@RequestMapping(value = "/rollback", method={ RequestMethod.POST })
	public ResponseEntity<Exception> rollback(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			conn.rollback();
		}
		catch(Exception e) {
			logg.error(e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/commit", method={ RequestMethod.POST })
	public ResponseEntity<Exception> commit(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			conn.commit();
		}
		catch(Exception e) {
			logg.error(e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setAutoCommit", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setAutoCommit(
			@RequestHeader Map<String, String> headers, 
			@RequestParam("flag") String flag) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he+", flag="+flag);
		try {
			JDBCSConnection conn = ManagerConnections.lookup(he);
			if(conn == null) {
				new Exception("Connection-objekt existerar ej");
			}
			conn.setAutoCommit(Boolean.parseBoolean(flag));
		}
		catch(Exception e) {
			logg.error(e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
