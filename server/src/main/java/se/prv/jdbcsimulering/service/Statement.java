package se.prv.jdbcsimulering.service;

import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.prv.jdbcsimulering.pool.ManagerStatements;
import se.prv.jdbcsimulering.serverobjects.JDBCSStatement;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;

@RestController
@RequestMapping(value = "/Statement")
public class Statement {
	
	private static Logger logg = Logger.getLogger(Statement.class); 

	private static String jwtsecret = null;
	
	static {
		jwtsecret = JwtTokenUtil.jwtsecret;
	}

	@RequestMapping(value = "/close", method={ RequestMethod.POST })
	public ResponseEntity<Exception> close(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSStatement stmt = ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("Statement-objekt existerar ej");
			}
			stmt.close();
		}
		catch(Exception e) {
			logg.error("Fel vid Statement.close", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

}
