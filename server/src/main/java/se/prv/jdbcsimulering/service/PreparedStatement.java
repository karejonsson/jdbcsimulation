package se.prv.jdbcsimulering.service;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.prv.jdbcsimulering.pool.ManagerConnections;
import se.prv.jdbcsimulering.pool.ManagerResultSets;
import se.prv.jdbcsimulering.pool.ManagerStatements;
import se.prv.jdbcsimulering.serverobjects.JDBCSConnection;
import se.prv.jdbcsimulering.serverobjects.JDBCSPreparedStatement;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSet;
import se.prv.jdbcsimulering.serverobjects.JDBCSStatement;
import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;
import se.prv.jdbcsimulering.utils.SerializationUtil;
import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;

@RestController
@RequestMapping(value = "/PreparedStatement")
public class PreparedStatement {
	
	private static Logger logg = Logger.getLogger(PreparedStatement.class); 

	private static String jwtsecret = null;
	
	static {
		jwtsecret = JwtTokenUtil.jwtsecret;
	}

	@RequestMapping(value = "/cancel", method={ RequestMethod.POST })
	public ResponseEntity<Exception> cancel(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.cancel();
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.cancel", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/clearBatch", method={ RequestMethod.POST })
	public ResponseEntity<Exception> clearBatch(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.clearBatch();
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.clearBatch", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/clearWarnings", method={ RequestMethod.POST })
	public ResponseEntity<Exception> clearWarnings(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.clearWarnings();
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.clearWarnings", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/close", method={ RequestMethod.POST })
	public ResponseEntity<Exception> close(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.close();
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.close", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/closeOnCompletion", method={ RequestMethod.POST })
	public ResponseEntity<Exception> closeOnCompletion(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.closeOnCompletion();
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.closeOnCompletion", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setDouble", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setDouble(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") double val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setDouble(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setDouble", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setInt", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setInt(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") int val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setInt(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setInt", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setFloat", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setFloat(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") float val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setFloat(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setFloat", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setLong", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setLong(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") long val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setLong(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setLong", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/setNull", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setNull(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("type") int type
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setNull(pos, type);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setNull", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setString", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setString(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") String val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setString(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setString", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setNString", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setNString(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") String val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setNString(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setNString", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setDate", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setDate(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("valmillis") String valmillis
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setDate(pos, new java.sql.Date(Long.parseLong(valmillis)));
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setDate", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setObject", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setObject(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("obj") String obj
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			Object restoredObj = SerializationUtil.deserialize(Base64.getDecoder().decode(obj));
			stmt.setObject(pos, restoredObj);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setObject", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/setShort", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setShort(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") short val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setShort(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setShort", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setByte", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setByte(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") byte val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setByte(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setByte", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}

	@RequestMapping(value = "/setBytes", method={ RequestMethod.POST })
	public ResponseEntity<Exception> setBytes(
			@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos,
			@RequestParam("val") byte[] val
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			stmt.setBytes(pos, val);
		}
		catch(Exception e) {
			logg.error("Fel vid PreparedStatement.setBytes", e);
			return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(null, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/execute", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> execute(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		boolean out = false;
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			out = stmt.execute();
		}
		catch(Exception e) {
			logg.error("Inget execute-svar från uppletad PreparedStatement", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.booleanReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/executeSql", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> executeSql(
			@RequestHeader Map<String, String> headers,
			@RequestParam("sql") String sql) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		boolean out = false;
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			out = stmt.execute(SymetricEncryptionUtil.decrypt(sql, jwtsecret));
		}
		catch(Exception e) {
			logg.error("Inget executeSql-svar från uppletad PreparedStatement", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.booleanReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/executeUpdate", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> executeUpdate(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		int out = 0;
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("PreparedStatement-objekt existerar ej");
			}
			out = stmt.executeUpdate();
		}
		catch(Exception e) {
			logg.error("Inget executeUpdate-svar från uppletad PreparedStatement", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.integerReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/executeQuery", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> executeQuery(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		JDBCSResultSet rs = null;
		try {
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("Connection-objekt existerar ej");
			}
			rs = (JDBCSResultSet) stmt.executeQuery();
		}
		catch(Exception e) {
			logg.error("Inget statement från uppletad Connection", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    if(rs == null) {
    		logg.warn("Fick inget ResultSet");
	    	return ResponseEntity.noContent().build();
	    }
	    try {
		    Map<String, Object> out = new HashMap<>();
		    try {
		    	String ref = rs.getReference();
		    	logg.info("Ref="+ref);
			    out.put(Constants.reference, SymetricEncryptionUtil.encrypt(ref, he.secret));
		    }
		    catch(Exception e) {
	    		logg.error("FEL", e);
	    		out.put(Constants.exception, e);
		    	return new ResponseEntity<>(out, HttpStatus.BAD_REQUEST);
		    }
	    	logg.info("ok");
			return new ResponseEntity<>(out, HttpStatus.OK);
	    }
	    catch(Exception e) {
	    	logg.error(e);
	    }
	    return null;
	}

	@RequestMapping(value = "/executeQuerySql", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> executeQuerySql(
			@RequestHeader Map<String, String> headers,
			@RequestParam("sql") String encryptedSql) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		JDBCSResultSet rs = null;
		try {
			String sql = SymetricEncryptionUtil.decrypt(encryptedSql, he.secret);
			JDBCSPreparedStatement stmt = (JDBCSPreparedStatement) ManagerStatements.lookup(he);
			if(stmt == null) {
				new Exception("Connection-objekt existerar ej");
			}
			rs = (JDBCSResultSet) stmt.executeQuery(sql);
		}
		catch(Exception e) {
			logg.error("Inget statement från uppletad Connection", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    if(rs == null) {
    		logg.warn("Fick inget ResultSet");
	    	return ResponseEntity.noContent().build();
	    }
	    try {
		    Map<String, Object> out = new HashMap<>();
		    try {
		    	String ref = rs.getReference();
		    	logg.info("Ref="+ref);
			    out.put(Constants.reference, SymetricEncryptionUtil.encrypt(ref, he.secret));
		    }
		    catch(Exception e) {
	    		logg.error("FEL", e);
	    		out.put(Constants.exception, e);
		    	return new ResponseEntity<>(out, HttpStatus.BAD_REQUEST);
		    }
	    	logg.info("ok");
			return new ResponseEntity<>(out, HttpStatus.OK);
	    }
	    catch(Exception e) {
	    	logg.error(e);
	    }
	    return null;
	}

}

