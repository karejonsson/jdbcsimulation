package se.prv.jdbcsimulering.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.prv.jdbcsimulering.pool.ManagerResultSetMetaDatas;
import se.prv.jdbcsimulering.pool.ManagerResultSets;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSet;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSetMetaData;
import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;

@RestController
@RequestMapping(value = "/ResultSetMetaData")
public class ResultSetMetaData {

	private static Logger logg = Logger.getLogger(ResultSetMetaData.class); 

	@RequestMapping(value = "/getColumnCount", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getColumnCount(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		int out = 0;
		try {
			JDBCSResultSetMetaData rsmd = (JDBCSResultSetMetaData) ManagerResultSetMetaDatas.lookup(he);
			if(rsmd == null) {
				new Exception("ResultSetMetaData-objekt existerar ej");
			}
			out = rsmd.getColumnCount();
		}
		catch(Exception e) {
			logg.error("Inget getColumnCount-svar från uppletat ResultSetMetaData", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.integerReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getColumnLabel", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getColumnLabel(@RequestHeader Map<String, String> headers,
			@RequestParam("column") int column
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		String out = null;
		try {
			JDBCSResultSetMetaData rsmd = (JDBCSResultSetMetaData) ManagerResultSetMetaDatas.lookup(he);
			if(rsmd == null) {
				new Exception("ResultSetMetaData-objekt existerar ej");
			}
			out = rsmd.getColumnLabel(column);
		}
		catch(Exception e) {
			logg.error("Inget getColumnLabel-svar från uppletat ResultSetMetaData", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.stringReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getColumnName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getColumnName(@RequestHeader Map<String, String> headers,
			@RequestParam("column") int column
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		String out = null;
		try {
			JDBCSResultSetMetaData rsmd = (JDBCSResultSetMetaData) ManagerResultSetMetaDatas.lookup(he);
			if(rsmd == null) {
				new Exception("ResultSetMetaData-objekt existerar ej");
			}
			out = rsmd.getColumnName(column);
		}
		catch(Exception e) {
			logg.error("Inget getColumnName-svar från uppletat ResultSetMetaData", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.stringReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getColumnType", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getColumnType(@RequestHeader Map<String, String> headers,
			@RequestParam("column") int column
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		int out = 0;
		try {
			JDBCSResultSetMetaData rsmd = (JDBCSResultSetMetaData) ManagerResultSetMetaDatas.lookup(he);
			if(rsmd == null) {
				new Exception("ResultSetMetaData-objekt existerar ej");
			}
			out = rsmd.getColumnType(column);
		}
		catch(Exception e) {
			logg.error("Inget getColumnType-svar från uppletat ResultSetMetaData", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.integerReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getColumnTypeName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getColumnTypeName(@RequestHeader Map<String, String> headers,
			@RequestParam("column") int column
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		String out = null;
		try {
			JDBCSResultSetMetaData rsmd = (JDBCSResultSetMetaData) ManagerResultSetMetaDatas.lookup(he);
			if(rsmd == null) {
				new Exception("ResultSetMetaData-objekt existerar ej");
			}
			out = rsmd.getColumnTypeName(column);
		}
		catch(Exception e) {
			logg.error("Inget getColumnTypeName-svar från uppletat ResultSetMetaData", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.stringReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

}
