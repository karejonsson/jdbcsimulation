package se.prv.jdbcsimulering.service;

import java.io.InputStream;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import general.reuse.properties.HarddriveProperties;
import se.prv.jdbcsimulering.pool.Purger;

@SpringBootApplication
public class JDBCWebService extends SpringBootServletInitializer {

    private static Logger logger = null;

    static {
    	try {
    		InputStream is = HarddriveProperties.getStream("jdbcwebservice_log4j.properties", JDBCWebService.class);
    		if(is != null) {
     			PropertyConfigurator.configure(is);
    		}
    		else {
        		is = HarddriveProperties.getStream("log4j.properties", JDBCWebService.class);
        		if(is != null) {
         			PropertyConfigurator.configure(is);
        		}  			
    		}
    	}
    	catch(Exception e) { }
    	logger = Logger.getLogger("jdbcwebservice");
    }

    public static void main(String[] args) {
    	Purger.start();
        SpringApplication.run(JDBCWebService.class, args);
    }
    
}