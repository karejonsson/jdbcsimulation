package se.prv.jdbcsimulering.service;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import se.prv.jdbcsimulering.pool.ManagerResultSets;
import se.prv.jdbcsimulering.pool.ManagerStatements;
import se.prv.jdbcsimulering.serverobjects.JDBCSPreparedStatement;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSet;
import se.prv.jdbcsimulering.serverobjects.JDBCSResultSetMetaData;
import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;
import se.prv.jdbcsimulering.utils.SerializationUtil;
import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;

@RestController
@RequestMapping(value = "/ResultSet")
public class ResultSet {
	
	private static Logger logg = Logger.getLogger(ResultSet.class); 

	@RequestMapping(value = "/next", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> next(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		boolean out = false;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.next();
		}
		catch(Exception e) {
			logg.error("Inget next-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.booleanReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/previous", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> previous(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		boolean out = false;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.previous();
		}
		catch(Exception e) {
			logg.error("Inget previous-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.booleanReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getInt", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getInt(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		int out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getInt(pos);
		}
		catch(Exception e) {
			logg.error("Inget getInt-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.integerReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getIntName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getIntName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		int out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getInt(name);
		}
		catch(Exception e) {
			logg.error("Inget getIntName-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.integerReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getLong", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getLong(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		long out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getLong(pos);
		}
		catch(Exception e) {
			logg.error("Inget getLong-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.longReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getLongName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getLongName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		long out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getLong(name);
		}
		catch(Exception e) {
			logg.error("Inget getLongName-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.longReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getShort", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getShort(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		short out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getShort(pos);
		}
		catch(Exception e) {
			logg.error("Inget getShort-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.shortReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getShortName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getShortName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		short out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getShort(name);
		}
		catch(Exception e) {
			logg.error("Inget getShortName-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.shortReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getString", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getString(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		String out = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getString(pos);
		}
		catch(Exception e) {
			logg.error("Inget getString-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.stringReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getStringName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getStringName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		String out = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getString(name);
		}
		catch(Exception e) {
			logg.error("Inget getStringName-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.stringReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getDouble", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getDouble(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		double out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getDouble(pos);
		}
		catch(Exception e) {
			logg.error("Inget getDouble-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.doubleReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getDoubleName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getDoubleName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		double out = 0;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getDouble(name);
		}
		catch(Exception e) {
			logg.error("Inget getDoubleName-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.doubleReturnValue, out);
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getDate", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getDate(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		Date out = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getDate(pos);
		}
		catch(Exception e) {
			logg.error("Inget getDate-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.dateReturnValue, out.getTime());
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getDateName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getDateName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		Date out = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getDate(name);
		}
		catch(Exception e) {
			logg.error("Inget getDateName-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    m.put(Constants.dateReturnValue, out.getTime());
		return new ResponseEntity<>(m, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/getObject", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getObject(@RequestHeader Map<String, String> headers,
			@RequestParam("pos") int pos
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		Object out = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getObject(pos);
		}
		catch(Exception e) {
			logg.error("Inget getObject-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    try {
			m.put(Constants.objectReturnValue, SerializationUtil.base64(out));
		} 
	    catch (IOException e) {
			logg.error("Kunde inte serialisera objektet "+out, e);
		    m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getObjectName", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getObjectName(@RequestHeader Map<String, String> headers,
			@RequestParam("name") String name
			) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		Object out = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			out = rs.getObject(name);
		}
		catch(Exception e) {
			logg.error("Inget getObject-svar från uppletat ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    Map<String, Object> m = new HashMap<>();
	    try {
			m.put(Constants.objectReturnValue, SerializationUtil.base64(out));
		} 
	    catch (IOException e) {
			logg.error("Kunde inte serialisera objektet "+out, e);
		    m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(m, HttpStatus.OK);
	}

	@RequestMapping(value = "/getMetaData", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getMetaData(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("header="+he);
		JDBCSResultSetMetaData rsmd = null;
		try {
			JDBCSResultSet rs = (JDBCSResultSet) ManagerResultSets.lookup(he);
			if(rs == null) {
				new Exception("ResultSet-objekt existerar ej");
			}
			rsmd = (JDBCSResultSetMetaData) rs.getMetaData();
		}
		catch(Exception e) {
			logg.error("Inget ResultSetMetaData från uppletad ResultSet", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    if(rsmd == null) {
    		logg.warn("Fick inget ResultSetMetaData");
	    	return ResponseEntity.noContent().build();
	    }
	    try {
		    Map<String, Object> out = new HashMap<>();
		    try {
		    	String ref = rsmd.getReference();
		    	logg.info("Ref="+ref);
			    out.put(Constants.reference, SymetricEncryptionUtil.encrypt(ref, he.secret));
		    }
		    catch(Exception e) {
	    		logg.error("FEL", e);
	    		out.put(Constants.exception, e);
		    	return new ResponseEntity<>(out, HttpStatus.BAD_REQUEST);
		    }
	    	logg.info("ok");
			return new ResponseEntity<>(out, HttpStatus.OK);
	    }
	    catch(Exception e) {
	    	logg.error(e);
	    }
	    return null;
	}

}
