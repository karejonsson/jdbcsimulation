package se.prv.jdbcsimulering.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import se.prv.jdbcsimulering.pool.ManagerConnections;
import se.prv.jdbcsimulering.serverobjects.JDBCSConnection;
import se.prv.jdbcsimulering.strings.Constants;
import se.prv.jdbcsimulering.util.HeaderExtract;
import se.prv.jdbcsimulering.util.HeaderUtil;
import se.prv.jdbcsimulering.utils.JwtTokenUtil;
import se.prv.jdbcsimulering.utils.SymetricEncryptionUtil;

@RestController
@RequestMapping(value = "/WebserviceManager")
public class WebserviceManager {

	private static Logger logg = Logger.getLogger(WebserviceManager.class); 
	
	private static String jwtsecret = null;
	
	static {
		jwtsecret = JwtTokenUtil.jwtsecret;
	}

	@RequestMapping(value = "/getConnection", method={ RequestMethod.POST })
	public ResponseEntity<Map<String, Object>> getConnection(@RequestHeader Map<String, String> headers) {
		HeaderExtract he = HeaderUtil.getExtract(headers);
		logg.info("WebserviceManager.getConnection(), header="+he);
	    JDBCSConnection conn = null;
	    try {
	    	conn = ManagerConnections.newConnection();
	    }
		catch(Exception e) {
			logg.error("Inget Connection kunde erhållas", e);
		    Map<String, Object> m = new HashMap<>();
		    m.put(Constants.exception, e);
			return new ResponseEntity<>(m, HttpStatus.BAD_REQUEST);
		}
	    if(conn == null) {
    		logg.warn("Fick ingen connection");
	    	return ResponseEntity.noContent().build();
	    }
	    Map<String, Object> out = new HashMap<>();
	    try {
	    	String ref = conn.getReference();
	    	logg.info("Ref="+ref);
		    out.put(Constants.reference, SymetricEncryptionUtil.encrypt(ref, he.secret));
	    }
	    catch(Exception e) {
    		logg.error("FEL", e);
	    	return ResponseEntity.noContent().build();
	    }
    	logg.info("ok");
		return new ResponseEntity<>(out, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/test/ping/post", method={RequestMethod.POST})
	public String ping_post() {
		logg.trace("ping_post()");
		return "pong post";
	}

	@RequestMapping(value = "/test/ping/get", method={RequestMethod.GET})
	public String ping_get() {
		logg.trace("ping_get()");
		return "pong get";
	}


}
